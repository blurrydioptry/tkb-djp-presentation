$(".datepick").flatpickr();

// button upload perpanjangan st stan
$(".btnUploadStPerpanjanganSt").click(function () {
    $("#upload_st").modal("show");
    $("#id_perpanjangan_stan").val(
        $(this).data("id_perpanjangan_stan_upload_st")
    );
});

// button lihat dasar perpanjangan st
$(".btnLihat").click(function () {
    $("#lihat_dasar_st").modal("show");

    $("#alasan_perpanjangan_st_lihat").val(
        $(this).data("alasan_perpanjangan_st_lihat")
    );
    $("#tgl_selesai_perpanjangan_lihat").val(
        $(this).data("tgl_selesai_perpanjangan_lihat")
    );
    $("#nomor_st_awal_lihat").val($(this).data("no_st_awal_lihat"));
    $("#file_dokumen_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_lihat")
    );
    $("#keterangan_lihat").val($(this).data("ket_lihat"));
    $("#file_dokumen_pendukung_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("file_pendukung_lihat")
    );
});

// button edit dasar perpanjangan st
$(".btnEdit").click(function () {
    $("#edit_dasar_st").modal("show");

    $("#id_perpanjangan_stan_edit").val(
        $(this).data("id_perpanjangan_stan_edit")
    );
    $("#alasan_perpanjangan_st_edit").val(
        $(this).data("alasan_perpanjangan_st_edit")
    );
    $("#tgl_selesai_perpanjangan_edit").val(
        $(this).data("tgl_selesai_perpanjangan_edit")
    );
    $("#nomor_st_awal_edit").val($(this).data("no_st_awal_edit"));
    $("#file_dokumen_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("file_edit")
    );
    $("#keterangan_edit").val($(this).data("ket_edit"));
    $("#file_dokumen_pendukung_edit").attr(
        "href",
        app_url + "/files/" + $(this).data("file_pendukung_edit")
    );
});

// button hapus perguruan tinggi
$(".btnHapus").click(function () {
    var id = $(this).data("id_perpanjangan_stan_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/tubel/perpanjanganststan/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
