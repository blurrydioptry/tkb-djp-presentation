$(".datepick").flatpickr();

// button rekam kep
$(".btnRekamKepPembebasan").click(function () {
    $("#rekam_kep_pembebasan").modal("show");

    $("#id_permohonan_aktif").val($(this).data("id_permohonan_aktif")).change();

    $("#nip_rekam").text(": " + $(this).data("nip"));
    $("#nama_rekam").text(": " + $(this).data("nama"));
    $("#tgl_aktif_kembali_rekam").text(": " + $(this).data("tglaktifkembali"));
    $("#nomor_st_rekam").text(": " + $(this).data("nomorst"));
    $("#tgl_st_rekam").text(": " + $(this).data("tglst"));
    // $("#tgl_mulai_selesai_rekam").text(": " + $(this).data('tglmulaist') + " s.d. " + $(this).data('tglselesaist'))
    $("#tgl_mulai_rekam").text(": " + $(this).data("tglmulaist"));
    $("#tgl_selesai_rekam").text(": " + $(this).data("tglselesaist"));
});

// button lihat kep
$(".btnLihatKepPembebasan").click(function () {
    $("#lihat_kep_pembebasan").modal("show");

    $("#nip_lihat").text(": " + $(this).data("nip"));
    $("#nama_lihat").text(": " + $(this).data("nama"));
    $("#tgl_aktif_kembali_lihat").text(": " + $(this).data("tglaktifkembali"));
    $("#nomor_st_lihat").text(": " + $(this).data("nomorst"));
    $("#tgl_st_lihat").text(": " + $(this).data("tglst"));
    // $("#tgl_mulai_selesai_lihat").text(": " + $(this).data('tglmulaist') + " s.d. " + $(this).data('tglselesaist'))
    $("#tgl_mulai_lihat").text(": " + $(this).data("tglmulaist"));
    $("#tgl_selesai_lihat").text(": " + $(this).data("tglselesaist"));

    $("#nomor_surat_tugas_lihat").val($(this).data("nomorkep"));
    $("#tgl_surat_tugas_lihat").val($(this).data("tglkep"));
    $("#tmt_lihat").val($(this).data("tmt"));
    $("#file_kep").attr("href", app_url + "/files/" + $(this).data("file"));
});

// placeholder advance search
$(window).on("load", function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: true,
        minimumResultsForSearch: -1,
    });
});

// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
