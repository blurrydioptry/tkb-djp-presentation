$(".datepick").flatpickr();
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});

// button rekam kep
$(".btnRekamKep").click(function () {
    $("#rekam_kep_cuti").modal("show");

    $("#id_cuti").val($(this).data("idcuti")).change();
    $("#namaRekam").text(": " + $(this).data("nama"));
    $("#nipRekam").text(": " + $(this).data("nip"));
    $("#tglMulai").text(": " + $(this).data("tglmulai"));
    $("#tglSelesai").text(": " + $(this).data("tglselesai"));
});

// button lihat kep
$(".btnLihatKep").click(function () {
    $("#lihat_kep_cuti").modal("show");

    $("#namaLihat").text(": " + $(this).data("nama"));
    $("#nipLihat").text(": " + $(this).data("nip"));
    $("#tglMulaiLihat").text(": " + $(this).data("tglmulai"));
    $("#tglSelesaiLihat").text(": " + $(this).data("tglselesai"));

    $("#nomor_kep_lihat").val($(this).data("nomorkep"));
    $("#tgl_kep_lihat").val($(this).data("tglkep"));
    $("#tmt_penempatan_lihat").val($(this).data("tgltmt"));
    $("#lokasi_penempatan_lihat").val($(this).data("lokasipenempatan"));
    $("#file_kep_lihat").attr(
        "href",
        app_url + "/files/" + $(this).data("filesuratkampus")
    );
});

$(document).ready(function () {
    $("#get_lokasi_penempatan").select2({
        minimumInputLength: 4,
        ajax: {
            type: "get",
            delay: 250,
            url: app_url + "/tubel/kep-penempatan-cuti/get-lokasi-penempatan",
            data: function (params) {
                return {
                    lokasi: params.term,
                };
            },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data,
                };
            },
            cache: true,
        },
    });
});

// get kantor iam
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#lokasi_penempatan_rekam").select2({
        placeholder: "Pilih lokasi penempatan",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/administrasi/get-kantor",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});

// $.fn.modal.Constructor.prototype.enforceFocus = function() {};

// placeholder advance search
$(document).ready(function () {
    // jenjang
    $selectElement = $("#search_jenjang").select2({
        placeholder: "jenjang pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });

    // lokasi
    $selectElement = $("#search_lokasi").select2({
        placeholder: "lokasi pendidikan",
        allowClear: false,
        minimumResultsForSearch: -1,
    });
});

// get kantor iam
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#kantorAsal").select2({
        placeholder: "kantor asal",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/kep-penempatan-cuti/get-kantor",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});

// get jabatan iam
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#jabatanAsal").select2({
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/tubel/kep-penempatan-cuti/get-jabatan",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});
