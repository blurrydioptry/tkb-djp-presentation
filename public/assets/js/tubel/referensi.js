$(".datepick").flatpickr();


// button lihat dasar referensi negara
$(".btnLihat").click(function () {
    $("#lihat_dasar_ref").modal("show");

    $("#id_ref_b").val(
        $(this).data("id_ref")
    );

    $("#nama_ref_b").val(
        $(this).data("nama_ref")
    );
    $("#tgl_ref_b").val(
        $(this).data("tgl_ref")
    );

});

// button edit dasar referensi
$(".btnEdit").click(function () {
    $("#edit_dasar_ref").modal("show");

    $("#id_ref_edit").val(
        $(this).data("id_ref")
    );
    $("#nama_ref_edit").val(
        $(this).data("nama_ref")
    );
});

// button hapus negara
$(".btnHapus").click(function () {
    var id = $(this).data("id_ref_negara_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/referensi/negara/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus perguruan tinggi
$(".btnHapusPT").click(function () {
    var id = $(this).data("id_ref_negara_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/referensi/pt/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus program studi
$(".btnHapusProdi").click(function () {
    var id = $(this).data("id_ref_prodi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/referensi/prodi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus jenjang pendidikan
$(".btnHapusJenjang").click(function () {
    var id = $(this).data("id_ref_jenjang_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/referensi/jenjang/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});

// button hapus lokasi pendidikan
$(".btnHapusLokasi").click(function () {
    var id = $(this).data("id_ref_lokasi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/referensi/lokasi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});



// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});
