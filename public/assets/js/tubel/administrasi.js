$(document).ready(function () {
    $("#btnTarikData").click(function () {
        var $this = $(this);

        //Call Button Loading Function
        BtnLoading($this);

        let url = app_url + "/tubel/administrasi/get-tubel";

        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                if (response.status == 0) {
                    swalError(response.message);
                } else {
                    setTimeout(function () {
                        $(this).prop("disabled", true);
                        swalSuccess(response.data.message);
                    }, location.reload());
                }

                // reset button
                BtnReset($this);
            },
        });
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
}

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}
