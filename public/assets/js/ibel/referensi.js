$(".datepick").flatpickr();

// button lihat dasar referensi
$(".btnLihat").click(function () {
    $("#lihat_dasar_ref").modal("show");

    $("#id_ref_b").val($(this).data("id_ref"));

    $("#nama_ref_b").val($(this).data("nama_ref"));

    $("#alamat_ref_b").val($(this).data("alamat_ref"));
    $("#tgl_ref_b").val($(this).data("tgl_ref"));

    //lihat lokasi pendidikan
    $("#nama_pt").val($(this).data("nama_pt"));
    $("#alamat_pt").val($(this).data("alamat_pt"));
    $("#nama_kampus").val($(this).data("nama_kampus"));
    $("#lokasi").val($(this).data("alamat"));
    $("#long").val($(this).data("long"));
    $("#lat").val($(this).data("lat"));
    //lihat akreditasi prodi
    $("#nama_pt_akr").val($(this).data("nama_pt_akr"));
    $("#alamat_pt").val($(this).data("alamat_pt"));
    $("#nama_prodi_akr").val($(this).data("nama_prodi_akr"));
    $("#jenjang_akr_prodi").val($(this).data("jenjang_pendidikan_akr"));
    $("#akreditasi_prodi").val($(this).data("akreditasi"));
    $("#tgl_berakhir_akr").val($(this).data("tgl_berakhir"));
    //lihat otoritas
    $("#nama_pegawai_otoritas").val($(this).data("nama_pegawai_oto"));
    $("#nama_kantor_otoritas").val($(this).data("nama_kantor_oto"));
    $("#nip_otoritas").val($(this).data("nip_oto"));
    $("#tipe_otoritas").val($(this).data("tipe_oto"));
});

// button edit dasar referensi
$(".btnEdit").click(function () {
    $("#edit_dasar_ref").modal("show");

    $("#id_ref_edit").val($(this).data("id_ref"));
    $("#nama_ref_edit").val($(this).data("nama_ref"));
    $("#alamat_ref_edit").val($(this).data("alamat_ref"));
    // $("#JenjangPendidikan").val(
    //     $(this).data("jenjang_id")
    // );
    var idJenjangprodi = $(this).data("jenjang_id");
    $("#JenjangPendidikanprodi").val(idJenjangprodi).change();

    //edit lokasi pendidikan
    $("#nama_kampus_edit").val($(this).data("nama_kampus"));
    $("#nama_pt_ref_lokasi").val($(this).data("nama_pt_ref_lokasi"));
    $("#lokasi_edit").val($(this).data("alamat"));
    $("#long_edit").val($(this).data("long"));
    $("#lat_edit").val($(this).data("lat"));
    var idPT = $(this).data("id_pt");
    $("#pt_lokasi_kuliah").val(idPT).change();

    //edit prodi akreditasi
    var id_pt_akr = $(this).data("id_pt_akr");
    $("#id_pt_akr").val(id_pt_akr).change();
    $("#id_pt_akr2").val($(this).data("id_pt_akr"));
    var id_prodi_akr = $(this).data("id_prodi_akr");
    $("#id_prodi_akr").val(id_prodi_akr).change();
    $("#id_prodi_akr2").val($(this).data("id_prodi_akr"));
    var id_akreditasi = $(this).data("id_akreditasi");
    $("#id_akreditasi_edit").val(id_akreditasi).change();
    var tgl_berakhir_akr = $(this).data("tgl_berakhir_akr");
    $("#tgl_berakhir_akr").val(tgl_berakhir_akr);

    //edit otorisasi
    $("#nama_pegawai_oto_edit").val($(this).data("nama_pegawai_oto"));
    $("#nama_pegawai_oto_edit_id").val($(this).data("nama_pegawai_oto_id"));
    $("#nama_kantor_oto_edit").val($(this).data("nama_kantor_oto"));
    $("#nama_kantor_oto_edit_id").val($(this).data("nama_kantor_oto_id"));
    $("#nip_oto_edit").val($(this).data("nip_oto"));
    var tipe_oto = $(this).data("tipe_oto");
    $("#tipe_oto_edit").val(tipe_oto).change();
});

// button hapus perguruan tinggi
$(".btnHapusPT").click(function () {
    var id = $(this).data("id_ref_pt_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/referensi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});
// button hapus program studi
$(".btnHapusProdi").click(function () {
    var id = $(this).data("id_ref_prodi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/referensi/prodi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});
// button hapus lokasi pendidikan
$(".btnHapusLokasi").click(function () {
    var id = $(this).data("id_ref_lokasi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/referensi/lokasi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});
// button hapus akreditasi
$(".btnHapusAkreditasi").click(function () {
    var id = $(this).data("id_ref_akreditasi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/referensi/prodiAkr/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});
// button hapus akreditasi
$(".btnHapusOtorisasi").click(function () {
    var id = $(this).data("id_ref_otorisasi_hapus");

    Swal.fire({
        title: "Yakin akan menghapus data?",
        text: "Data tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#C5584B",
        cancelButtonColor: "#B2B2B2",
        confirmButtonText: "Hapus!",
        cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: app_url + "/ibel/referensi/otorisasi/delete",
                data: {
                    _token: $("meta[name='csrf-token']").attr("content"),
                    id: id,
                },
                success: function (response) {
                    $("#" + id).remove();

                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: response.message,
                        showConfirmButton: false,
                        title: "Berhasil",
                        text: "data berhasil dihapus",
                        timer: 2000,
                    });
                },
                error: function (err) {
                    console.log(err);
                },
            });
        }
    });
});
// date format
$(".date-search").flatpickr({
    dateFormat: "d-m-Y",
});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    // get data kantor (add)
    $("#tambah_perguruan_tinggi").select2({
        placeholder: "Pilih kantor",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/ibel/referensi/otorisasi/get-kantor",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
    // get data kantor (add)
    $("#tambah_data_pegawai").select2({
        placeholder: "Pilih nama pegawai",
        minimumInputLength: 3,
        ajax: {
            url: app_url + "/ibel/referensi/otorisasi/get-nama",
            dataType: "json",
            type: "post",
            delay: 300,
            data: function (params) {
                return {
                    search: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response,
                };
            },
        },
    });
});
