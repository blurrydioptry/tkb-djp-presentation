$(document).ready(function () {
    $("#searchData").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#dataPermohonan tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

$(document).ready(function () {
    $("#searchDataUpkKanwil").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#dataReviewKanwil tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

// spinner / loading button
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('<div class="blockui-message"><span class="spinner-border text-warning spinner-border-sm"></span> Loading...</div>');
};

// spinner off / reset button
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
};

// block ui
var target = document.querySelector("#block");
var blockUI = new KTBlockUI(target, {
    message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
});

$(".btnAjukan").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");


    Swal.fire({
        title: 'Yakin akan mengajukan permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Ajukan!'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();

            $.ajax({
                type: 'patch',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });

                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil diajukan!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$(".btnHapus").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        title: 'Yakin akan menghapus permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        confirmButtonColor: '#F1416C',
        // confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Hapus!'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'delete',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil dihapus!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$(document).on("click", ".detilModal", function () {
    var idMpp = $(this).data('mpp');
    var tiket = $(this).data('tiket');
    var tanggal = $(this).data('tanggal');
    var keterangan = $(this).data('keterangan');
    var status = $(this).data('status');
    var page = '1';

    $(".modal-body #tiket").val(tiket);
    $(".modal-body #tanggal").val(tanggal);
    $(".modal-body #keterangan").val(keterangan);
    $(".modal-body #status").val(status);

    BtnLoading($("#example"));

    $.ajax({
        'url': "/permohonan-masa-pensiun/permohonan/log-status?page=" + page + "&idMpp=" + idMpp,
        'method': "GET",
        'contentType': 'application/json',
        "processing": true,

    }).done(function (data) {
        BtnReset($('#example'));
        var data = data['logMpp'];
        var eTable = '<tbody>';
        eTable += '<thead class="fw-bolder bg-secondary fs-6">';
        eTable += '<tr class="text-center">';
        eTable += '<th class="ps-4">Tanggal</th>';
        eTable += '<th class="">Keterangan</th>';
        eTable += '<th class="pe-4">Aktor</th>';
        eTable += '</tr>';
        eTable += '</thead>';
        $.each(data, function (i, item) {
            var date = new Date(item.createdDate);
            var tahun = date.getFullYear();
            var bulan = date.getMonth();
            var tanggal = date.getDate();

            switch (bulan) {
                case 0:
                    bulan = "Januari";
                    break;
                case 1:
                    bulan = "Februari";
                    break;
                case 2:
                    bulan = "Maret";
                    break;
                case 3:
                    bulan = "April";
                    break;
                case 4:
                    bulan = "Mei";
                    break;
                case 5:
                    bulan = "Juni";
                    break;
                case 6:
                    bulan = "Juli";
                    break;
                case 7:
                    bulan = "Agustus";
                    break;
                case 8:
                    bulan = "September";
                    break;
                case 9:
                    bulan = "Oktober";
                    break;
                case 10:
                    bulan = "November";
                    break;
                case 11:
                    bulan = "Desember";
                    break;
            }

            eTable += "<tr class='text-center'>";
            eTable += "<td>" + tanggal + " " + bulan + " " + tahun + "</td>";
            eTable += "<td>" + item.output + "</td>";
            eTable += "<td>" + item.namaAktor + '<span class="text-muted fw-bold text-muted d-block fs-7">' + item.nip18Aktor + "</span></td>";
            eTable += "</tr>";
        })
        eTable += '</tbody>';
        $('#example').html(eTable);
    })
});

$("#datemonth").datepicker({
    format: "yyyy-mm",
    startView: "year",
    minViewMode: "year",
    autoclose: true
});


$("#datemonth1").datepicker({
    format: "yyyy-mm",
    startView: "year",
    minViewMode: "year",
    autoclose: true
});

$(document).ready(function () {
    $('#uploadskpelanggarandisiplin').on('change', function () {
        $("#doneuploadskpelanggarandisiplin").html($('<span/>', {
            class: 'las la-file-pdf fs-3x pe-4'
        }));
        $("#skpelanggarandisiplin_text").text('SKet Disiplin');
        if ($("#uploadskpelanggarandisiplin").val() != "") {
            $(".hapusdoneuploadskpelanggarandisiplin").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskpelanggarandisiplin").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskpelanggarandisiplin").click(function () {
        $("#uploadskpelanggarandisiplin").val(null);
        $("#doneuploadskpelanggarandisiplin").text(null);
        $("#skpelanggarandisiplin_text").text('no file');
        $("#doneuploadskpelanggarandisiplin").html($('<span/>', {
            class: 'las la-file text-secondary fs-3x pe-4'
        }));
        $(".hapusdoneuploadskpelanggarandisiplin").attr("style", "display:none");
    })
});

$(document).ready(function () {
    $('#uploadskperadilan').on('change', function () {
        $("#doneuploadskperadilan").html($('<span/>', {
            class: 'las la-file-pdf fs-3x pe-4'
        }));
        $("#skperadilan_text").text('SKet Peradilan');
        if ($("#uploadskperadilan").val() != "") {
            $(".hapusdoneuploadskperadilan").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskperadilan").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskperadilan").click(function () {
        $("#uploadskperadilan").val(null);
        $("#doneuploadskperadilan").text(null);
        $("#skperadilan_text").text('no file');
        $("#doneuploadskperadilan").html($('<span/>', {
            class: 'las la-file text-secondary fs-3x pe-4'
        }));
        $(".hapusdoneuploadskperadilan").attr("style", "display:none");
    })
});

$(document).ready(function () {
    $('#uploadskselesaipekerjaan').on('change', function () {
        $("#doneuploadskselesaipekerjaan").html($('<span/>', {
            class: 'las la-file-pdf fs-3x pe-4'
        }));
        $("#skselesaipekerjaan_text").text('SKet Pekerjaan');

        if ($("#uploadskselesaipekerjaan").val() != "") {
            $(".hapusdoneuploadskselesaipekerjaan").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskselesaipekerjaan").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskselesaipekerjaan").click(function () {
        $("#uploadskselesaipekerjaan").val(null);
        $("#doneuploadskselesaipekerjaan").text(null);
        $("#skselesaipekerjaan_text").text('no file');
        $("#doneuploadskselesaipekerjaan").html($('<span/>', {
            class: 'las la-file text-secondary fs-3x pe-4'
        }));
        $(".hapusdoneuploadskselesaipekerjaan").attr("style", "display:none");
    })
});

$('#datemonth').on('change', function () {

    var start = $("#datemonth").val();
    var stop = $("#datemonth1").val();
    var valuestart = start.replace('-', '');
    var valueend = stop.replace('-', '');

    $(function () {
        // hitung selisih
        var months = ($('#datemonth1').datepicker('getDate').getFullYear() - $('#datemonth')
            .datepicker('getDate').getFullYear()) * 12;
        months -= $('#datemonth').datepicker('getDate').getMonth();
        months += $('#datemonth1').datepicker('getDate').getMonth();

        // validasi cek selisih
        if (months > 12) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Masa Persiapan Maksimal 12 Bulan !',
                confirmButtonColor: '#50CD89',
                confirmButtonText: 'Ok!',
            })

            $("#datemonth").val('');
            $('#selisihBulan').val('');

        } else if (months < 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Masa Persiapan Tidak Valid !',
                confirmButtonColor: '#50CD89',
                confirmButtonText: 'Ok!',
            })

            $("#datemonth").val('');
            $('#selisihBulan').val('');
        } else {
            $('#selisihBulan').val(months <= 0 ? 0 : months);
            $('#selisihbln').val(months <= 0 ? 0 : months);
        }
    })
});

$(document).ready(function () {
    $("#formadd").validate();
});

$(".btnTolak").click(function () {
    var id = $(this).data("id");
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
        input: 'textarea',
        inputLabel: 'Alasan Tolak',
        inputPlaceholder: 'Masukan Alasan Penolakan',
        inputAttributes: {
            'aria-label': 'Masukan Alasan Penolakan'
        },
        title: 'Yakin akan menolak data?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C5584B',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Tolak!',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                    id: id,
                    keterangan: result.value
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();

                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil ditolak',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        window.location.href = app_url + "/permohonan-masa-pensiun/review";
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });


        }
    })
});

$(".btnSetuju").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");
    Swal.fire({
        title: 'Yakin akan menyetujui permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Setujui!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil disetujui!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        window.location.href = app_url + "/permohonan-masa-pensiun/review";
                    }
                },
                error: function (err) {}
            });


        }
    })
});


$("#datemonthyear").datepicker({
    format: "yyyy-mm-dd",
    startView: "month",
    minViewMode: "month",
    autoclose: true
});

$(document).ready(function () {
    $('#uploadndskonfirmasi').on('change', function () {
        $("#doneuploadndskonfirmasi").text('FILE KONFIRMASI');

        if ($("#uploadndskonfirmasi").val() != "") {
            $(".hapusdoneuploadndskonfirmasi").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadndskonfirmasi").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadndskonfirmasi").click(function () {
        $("#uploadndskonfirmasi").val(null);
        $("#doneuploadndskonfirmasi").text(null);
        $(".hapusdoneuploadndskonfirmasi").attr("style", "display:none");
    })
});

$(document).ready(function () {
    $('#uploadndfilespermintaan').on('change', function () {
        $("#doneuploadndfilespermintaan").text('FILE PERMINTAAN');

        if ($("#uploadndfilespermintaan").val() != "") {
            $(".hapusdoneuploadndfilespermintaan").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadndfilespermintaan").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadndfilespermintaan").click(function () {
        $("#uploadndfilespermintaan").val(null);
        $("#doneuploadndfilespermintaan").text(null);
        $(".hapusdoneuploadndfilespermintaan").attr("style", "display:none");
    })
});



$(document).ready(function () {
    $('#uploadskMPP').on('change', function () {
        $("#doneuploadskMPP").html($('<span/>', {
            class: 'las la-file-pdf fs-3x pe-4'
        }));
        $("#skMPP_text").text('SK MPP.pdf');

        if ($("#uploadskMPP").val() != "") {
            $(".hapusdoneuploadskMPP").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskMPP").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskMPP").click(function () {
        $("#uploadskMPP").val(null);
        $("#doneuploadskMPP").text(null);
        $("#skMPP_text").text(null);
        $("#doneuploadskMPP").html($('<span/>', {
            class: ''
        }));
        $(".hapusdoneuploadskMPP").attr("style", "display:none");
    })
});

$(".btnSimpanFinal").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");
    Swal.fire({
        title: 'Yakin akan menyimpan hasil penelitian?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Simpan!'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });

                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil disimpan!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$(document).on("click", ".modalAttach", function () {

    var idMpp = $(this).data('mpp');
    var namapegawai = $(this).data('nmpeg');
    var tiket = $(this).data('tiket');
    var nip = $(this).data('nip');

    $(".modal-body #idMpp").val(idMpp);
    $(".modal-body #tiket").val(tiket);
    $(".modal-body #namapegawai").val(namapegawai);
    $(".modal-body #nip").val(nip);

});


$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#kirimSK').submit(function (e) {
        e.preventDefault();
        var token = $("meta[name='csrf-token']").attr("content");
        var formData = new FormData(this);
        var url = $(this).data('url');
        Swal.fire({
            title: 'Yakin akan mengirim file SK MPP?',
            icon: 'warning',
            reverseButtons: true,
            showCancelButton: true,
            // confirmButtonColor: '#F1416C',
            confirmButtonColor: '#50cd89',
            cancelButtonColor: '#B2B2B2',
            confirmButtonText: 'Kirim!'
        }).then((result) => {
            if (result.isConfirmed) {
                BtnLoading(".loadingBtn");
                $.ajax({
                    type: 'post',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response['status'] == 0) {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'Data berhasil dikirim!',
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                            location.reload();
                        }
                    },
                    error: function (err) {}
                });
            }
        })
    });
});


$("#reloadTolak").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);
});

$("#reloadTangguhkan").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);

});

$("#reloadSetuju").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);
});


$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#simpanDisposisi').submit(function (e) {
        e.preventDefault();
    var token = $("meta[name='csrf-token']").attr("content");
    var formData = new FormData(this);
    var url = $(this).data('url');
    Swal.fire({
        title: 'Yakin akan melakukan disposisi Permohonan MPP?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        cancelButtonText:'Tidak',
        confirmButtonText: 'Ya!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'post',
                url: url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });

                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Disposisi berhasil!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        window.location.href = app_url + "/permohonan-masa-pensiun/disposisi";

                    }
                },
                error: function (err) {}
            });
        }
    })
});
});
