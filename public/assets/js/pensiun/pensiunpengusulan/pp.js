$(document).ready(function () {
    $("#searchData").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#dataPermohonan tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

/* spinner / loading button */
function BtnLoading(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('<span class="spinner-border text-warning"></span>');
};

/* spinner off / reset button */
function BtnReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
};

$(document).on("click", ".detilModalStatus", function () {
    var idPp = $(this).data('pp');
    var tiket = $(this).data('tiket');
    var tanggal = $(this).data('tanggal');
    var keterangan = $(this).data('keterangan');
    var status = $(this).data('status');
    var page = '1';

    $(".modal-body #tiket").val(tiket);
    $(".modal-body #tanggal").val(tanggal);
    $(".modal-body #keterangan").val(keterangan);
    $(".modal-body #status").val(status);

    BtnLoading($("#example"));

    $.ajax({
        'url': "/pengusulan-pensiun/permohonan/log-status?page=" + page + "&idPp=" + idPp,
        'method': "GET",
        'contentType': 'application/json',
        "processing": true,

    }).done(function (data) {
        BtnReset($('#example'));
        var data = data['logPp'];
        var eTable = '<tbody>';
        eTable += '<thead class="fw-bolder bg-secondary fs-6">';
        eTable += '<tr class="text-center">';
        eTable += '<th class="ps-4">Tanggal</th>';
        eTable += '<th class="">Keterangan</th>';
        eTable += '<th class="pe-4">Aktor</th>';
        eTable += '</tr>';
        eTable += '</thead>';
        $.each(data, function (i, item) {
            var date = new Date(item.createdDate);
            var tahun = date.getFullYear();
            var bulan = date.getMonth();
            var tanggal = date.getDate();

            switch (bulan) {
                case 0:
                    bulan = "Januari";
                    break;
                case 1:
                    bulan = "Februari";
                    break;
                case 2:
                    bulan = "Maret";
                    break;
                case 3:
                    bulan = "April";
                    break;
                case 4:
                    bulan = "Mei";
                    break;
                case 5:
                    bulan = "Juni";
                    break;
                case 6:
                    bulan = "Juli";
                    break;
                case 7:
                    bulan = "Agustus";
                    break;
                case 8:
                    bulan = "September";
                    break;
                case 9:
                    bulan = "Oktober";
                    break;
                case 10:
                    bulan = "November";
                    break;
                case 11:
                    bulan = "Desember";
                    break;
            }

            eTable += "<tr class='text-center'>";
            eTable += "<td>" + tanggal + " " + bulan + " " + tahun + "</td>";
            eTable += "<td>" + item.output + "</td>";
            eTable += "<td>" + item.namaAktor + '<span class="text-muted fw-bold text-muted d-block fs-7">' + item.nip18Aktor + "</span></td>";
            eTable += "</tr>";
        })
        eTable += '</tbody>';
        $('#example').html(eTable);


    })
});

$(".btnAjukan").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");
    // block ui
    var target = document.querySelector(".block");
    var blockUI = new KTBlockUI(target, {
        message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
    });

    Swal.fire({
        title: 'Yakin akan mengajukan permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Ajukan!'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'patch',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });

                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil diajukan!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$(".btnHapus").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");
    // block ui
    var target = document.querySelector(".block");
    var blockUI = new KTBlockUI(target, {
        message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
    });


    Swal.fire({
        title: 'Yakin akan menghapus permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        confirmButtonColor: '#F1416C',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Hapus!'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'delete',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil dihapus!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$(document).ready(function () {
    $("#datepickerAdd").datepicker({
        format: "yyyy-MM-dd",
        startView: "year",
        minViewMode: "year",
        // autoclose: true,
    });
});

$('#datepickerAdd').on('change', function () {
    // mengambil last day of month
    var bulan = ($('#datepickerAdd').datepicker('getDate').getMonth());
    var tahun = ($('#datepickerAdd').datepicker('getDate').getFullYear());
    var akhirbulan = new Date(tahun, bulan + 1, 0);
    var akbulan = akhirbulan.getDate();
    var month = ("0" + ($('#datepickerAdd').datepicker('getDate').getMonth() + 1)).slice(-2)

    var fullakbulan = tahun + "-" + month + "-" + akbulan;
    $('.datepicker').hide();
    $("#datepickerAdd").val(fullakbulan);
});

$(document).ready(function () {
    $("#datepicker").datepicker({
        format: "yyyy-MM-dd",
        startView: "year",
        minViewMode: "year",
        // autoclose: true,
    });

    Inputmask({
        "mask": "9",
        "repeat": 9,
        "greedy": false
    }).mask(".formatInput");
});

$('#datepicker').on('change', function () {
    // mengambil last day of month
    var bulan = ($('#datepicker').datepicker('getDate').getMonth());
    var tahun = ($('#datepicker').datepicker('getDate').getFullYear());
    var akhirbulan = new Date(tahun, bulan + 1, 0);
    var akbulan = akhirbulan.getDate();
    var month = ("0" + ($('#datepicker').datepicker('getDate').getMonth() + 1)).slice(-2)

    var fullakbulan = tahun + "-" + month + "-" + akbulan;
    $('.datepicker').hide();
    $("#datepicker").val(fullakbulan);
});

function loadingEvent(elem) {
    $(elem).attr("data-original-text", $(elem).html());
    $(elem).prop("disabled", true);
    $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
}

// spinner off / reset button
function loadingReset(elem) {
    $(elem).prop("disabled", false);
    $(elem).html($(elem).attr("data-original-text"));
}

$("#btnGetPegawai").click(function () {
    if ($("#niprekam").val().length == 9) {
        var nip = $("#niprekam").val();
        loadingEvent(".loadingBtn");
        $.ajax({
            type: "get",
            url: app_url + "/pengusulan-pensiun/getDataPegawai/" + nip,
            data: {
                nip: nip,
            },
            success: function (response) {
                if (response.status == 0) {
                    Swal.fire({
                        position: "center",
                        icon: "error",
                        title: "Gagal!",
                        text: response.message,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    // console.log(response.message);
                } else {
                    // hidden parameter set
                    $("input[name=pegawaiId]").val(response.data.pegawaiId);
                    $("input[name=pangkatId]").val(response.data.pangkatId);
                    $("input[name=jabatanId]").val(response.data.jabatanId);
                    $("input[name=unitOrgId]").val(response.data.unitId);
                    $("input[name=namaUnitOrg]").val(response.data.unit);
                    $("input[name=kantorId]").val(response.data.kantorId);
                    $("input[name=pegawaiInputId]").val(response.data.pegawaiId);
                    $("input[name=kdKpp]").val(response.data.kdkpp);
                    $("input[name=kdKanwil]").val(response.data.kdkanwil);
                    $("input[name=kdKantor]").val(response.data.kantorLegacyKode);

                    // parameter utama
                    $("input[name=nama]").val(response.data.namaPegawai);
                    $("input[name=nip]").val(response.data.nip18);
                    $("input[name=jabatan]").val(response.data.jabatan);
                    $("input[name=unitorganisasi]").val(response.data.kantor);
                    $("input[name=pangkat]").val(response.data.pangkat);
                    var alamat = response.data.alamatJalan + " " + response.data.kelurahan + " " + response.data.kecamatan + " " + response.data.kabupaten + " " + response.data.provinsi;
                    $("textarea[name=alamat]").text(alamat);
                }
                loadingReset(".loadingBtn");
            },
        });
    } else {
        loadingReset(".loadingBtn");
        Swal.fire({
            position: "center",
            icon: "error",
            title: "Gagal!",
            text: "Jumlah karakter harus 9 digit",
            showConfirmButton: false,
            timer: 1500,
        });
    }
});

$('#selectJenis').on('change', function () {
    var id = this.value;

    $.ajax({
        type: 'get',
        url: "/pengusulan-pensiun/getDokumen/" + id + "/null",
        processing: true,

    }).done(function (data) {
        var data = data['data'];

        var src = "{{ url('assets') }}/src/media/svg/files/pdf.svg";
        // console.log(datas);
        var eTable = '<tbody>';
        eTable += '<thead class="fw-bolder bg-secondary">';
        eTable += '<tr class="fw-bold text-gray-800 border-bottom-2 border-gray-200 align-middle">';
        eTable += '<th width="50%">Keterangan</th>';
        eTable += '<th width="30%">Nomor Surat</th>';
        eTable += '<th width="20%">Aksi</th>';
        eTable += '</tr>';
        eTable += '</thead>';
        $.each(data, function (i, item) {
            var url = "/pengusulan-pensiun/generate/";
            eTable += "<tr>";
            eTable += '<td class="align-middle"> <label for="sp_berhenti_pns">' + item.idJnsDok.namaDok + '</label>'
            eTable += ' (<a href="' + url + item.idJnsDok.idJnsDok + '" data-bs-toggle="tooltip" data-bs-placement="top" title="Generate">Generate Dokumen</a>) '
            eTable += '</td>'
            eTable += '<td class="align-middle"> <input type="text" class="form-control" placeholder="Nomor Surat" name="surat[' + item.idJnsDok.idJnsDok + '][nomorSurat]" required></td>';
            eTable += '<td>'
            eTable += '<div class="col-6"><input type="file" name="surat[' + item.idJnsDok.idJnsDok + '][file]" class="custom-file-upload" required><span class="form-text text-muted">Max file size is 2MB.</span></div>'
            eTable += '</td>'
            eTable += "</tr>";
        })
        eTable += '</tbody>';
        $('#routeTable').html(eTable);


    })
});


$(document).on("click", ".detilModal", function () {
    var srce = $(this).data('url');
    $(".modal-body").html($('<iframe/>', {
        style: "width:100%;height:100%;",
        src: srce,
        frameborder: "1"
    }));
    var namafile = $(this).data('file');
    $(".modal-title").text("File " + namafile)
});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#simpanDisposisi').submit(function (e) {
        e.preventDefault();
        var token = $("meta[name='csrf-token']").attr("content");
        var formData = new FormData(this);
        var url = $(this).data('url');
        Swal.fire({
            title: 'Yakin akan melakukan disposisi Pengusulan Pensiun?',
            icon: 'warning',
            reverseButtons: true,
            showCancelButton: true,
            // confirmButtonColor: '#F1416C',
            confirmButtonColor: '#50cd89',
            cancelButtonColor: '#B2B2B2',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'post',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response['status'] == 0) {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                            });

                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'Disposisi berhasil!',
                                showConfirmButton: false,
                                timer: 1600
                            });
                            window.location.href = app_url + "/pengusulan-pensiun/permohonan/disposisi";

                        }
                    },
                    error: function (err) {}
                });
            }
        })
    });
});

$(".btnTolak").click(function () {
    var id = $(this).data("id");
    var url = $(this).data("url");

    // block ui
    var target = document.querySelector(".block");
    var blockUI = new KTBlockUI(target, {
        message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
    });

    var token = $("meta[name='csrf-token']").attr("content");
    // console.log(url);
    Swal.fire({
        input: 'textarea',
        inputLabel: 'Alasan Tolak',
        inputPlaceholder: 'Masukan Alasan Penolakan',
        inputAttributes: {
            'aria-label': 'Masukan Alasan Penolakan'
        },
        title: 'Yakin akan menolak permohonan ini?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C5584B',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Tolak!',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            //alert(result.value);
            blockUI.block();

            $.ajax({
                type: 'patch',
                url: url,
                data: {
                    _token: token,
                    id: id,
                    keterangan: result.value
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'data berhasil di tolak',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        window.location.href = app_url + "/pengusulan-pensiun/penelitian";
                        blockUI.release();
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });


        }
    })
});

$(".btnSetuju").click(function () {
    var url = $(this).data("url");
    var target = document.querySelector(".block");
    var blockUI = new KTBlockUI(target, {
        message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
    });

    var token = $("meta[name='csrf-token']").attr("content");
    Swal.fire({
        title: 'Yakin akan menyetujui permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Setujui!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil disetujui!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        window.location.href = app_url + "/pengusulan-pensiun/penelitian";
                    }
                },
                error: function (err) {}
            });


        }
    })
});

$("#reloadTolak").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);
});

$("#reloadTangguhkan").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);

});

$("#reloadSetuju").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);
});


$(".reloadSetuju").on("click", function () {
    var url = $(this).data("url");
    window.open(url);
    setTimeout(function () {
        location.reload();
    }, 1000);
});

$(document).on("click", ".detilModalAttach", function () {

    var idMpp = $(this).data('mpp');
    var namapegawai = $(this).data('nmpeg');
    var tiket = $(this).data('tiket');
    var nip = $(this).data('nip');

    $(".modal-body #idPp").val(idMpp);
    $(".modal-body #tiket").val(tiket);
    $(".modal-body #namapegawai").val(namapegawai);
    $(".modal-body #nip").val(nip);

});

$(document).on("click", ".detilModalSKPP", function () {

    var idMpp = $(this).data('mpp');
    var namapegawai = $(this).data('nmpeg');
    var tiket = $(this).data('tiket');
    var nip = $(this).data('nip');
    var url = "/pengusulan-pensiun/penelitian/generatekonsep/" + $(this).data('mpp');

    $(".modal-body #idPp").val(idMpp);
    $(".modal-body #tiket").val(tiket);
    $(".modal-body #namapegawai").val(namapegawai);
    $(".modal-body #nip").val(nip);
    // $(".modal-body #urlgetdata").addAttribute("data-url",url);
    document.getElementById("urlgetdata").setAttribute("data-url", url);


});

$(document).ready(function () {
    $('#uploadskMPP').on('change', function () {
        $("#doneuploadskMPP").text(this.files[0].name);

        if ($("#uploadskMPP").val() != "") {
            $(".hapusdoneuploadskMPP").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskMPP").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskMPP").click(function () {
        $("#uploadskMPP").val(null);
        $("#doneuploadskMPP").text(null);
        $(".hapusdoneuploadskMPP").attr("style", "display:none");
    })
});

$(document).ready(function () {
    $('#uploadskPP').on('change', function () {
        $("#doneuploadskPP").text(this.files[0].name);

        if ($("#uploadskPP").val() != "") {
            $(".hapusdoneuploadskPP").attr("style", "display:inline");
        } else {
            $(".hapusdoneuploadskPP").attr("style", "display:none");
        }
    })
    $(".hapusdoneuploadskPP").click(function () {
        $("#uploadskPP").val(null);
        $("#doneuploadskPP").text(null);
        $(".hapusdoneuploadskPP").attr("style", "display:none");
    })
});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#kirimSK').submit(function (e) {
        e.preventDefault();
        var token = $("meta[name='csrf-token']").attr("content");
        var formData = new FormData(this);
        var url = $(this).data('url');
        Swal.fire({
            title: 'Yakin akan mengirim file SK PP?',
            icon: 'warning',
            reverseButtons: true,
            showCancelButton: true,
            // confirmButtonColor: '#F1416C',
            confirmButtonColor: '#50cd89',
            cancelButtonColor: '#B2B2B2',
            confirmButtonText: 'Kirim!'
        }).then((result) => {
            if (result.isConfirmed) {
                BtnLoading(".loadingBtn");
                $.ajax({
                    type: 'post',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response['status'] == 0) {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'Data berhasil dikirim!',
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                            location.reload();
                        }
                    },
                    error: function (err) {}
                });
            }
        })
    });
});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#kirimSKPP').submit(function (e) {
        e.preventDefault();
        var token = $("meta[name='csrf-token']").attr("content");
        var formData = new FormData(this);
        var url = $(this).data('url');
        // console.log(url);
        Swal.fire({
            title: 'Yakin akan mengirim file SK PP?',
            icon: 'warning',
            reverseButtons: true,
            showCancelButton: true,
            // confirmButtonColor: '#F1416C',
            confirmButtonColor: '#50cd89',
            cancelButtonColor: '#B2B2B2',
            confirmButtonText: 'Kirim!'
        }).then((result) => {
            if (result.isConfirmed) {
                BtnLoading(".loadingBtn");
                $.ajax({
                    type: 'post',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response['status'] == 0) {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'Data berhasil dikirim!',
                                showConfirmButton: false,
                                timer: 1600
                            });
                            BtnReset(".loadingBtn");
                            location.reload();
                        }
                    },
                    error: function (err) {}
                });
            }
        })
    });
});

$(".btnProses").click(function () {
    var url = $(this).data("url");
    var token = $("meta[name='csrf-token']").attr("content");
    // block ui
    var target = document.querySelector(".block");
    var blockUI = new KTBlockUI(target, {
        message: '<div class="blockui-message"><span class="spinner-border text-warning"></span> Loading...</div>',
    });

    Swal.fire({
        title: 'Yakin akan memproses permohonan?',
        icon: 'warning',
        reverseButtons: true,
        showCancelButton: true,
        // confirmButtonColor: '#F1416C',
        confirmButtonColor: '#50cd89',
        cancelButtonColor: '#B2B2B2',
        confirmButtonText: 'Setuju!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.isConfirmed) {
            blockUI.block();
            $.ajax({
                type: 'patch',
                url: url,
                data: {
                    _token: token,
                },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                    } else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data berhasil diajukan!',
                            showConfirmButton: false,
                            timer: 1600
                        });
                        blockUI.release();
                        location.reload();
                    }
                },
                error: function (err) {}
            });


        }
    })
});
