<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <title>Dev |TKB DJP</title>

    @include('layout.header')

</head>


<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-2">
                    <!--begin::Post-->
                    <div class="content flex-row-fluid" id="kt_content">
                        <!--begin::Row-->
                        <div class="row gy-5 g-xl-4">
                            <!--begin::Col-->
                            <div class="col-xl">
                                <!--begin::Tables Widget 9-->
                                <div class="card card-xxl-stretch mb-xl-8">
                                    <div class="col-md-12">
                                        <!--begin::Body-->
                                        <div class="card shadow-sm m-2">
                                            <div class="card-header headercustom">
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bolder fs-3 text-white">Add
                                                        Knowledge</span>
                                                </h3>
                                            </div>
                                            <div class="row">
                                                <div class="text-end p-4">
                                                    <button type="button" class="btn btn-sm btn-primary"
                                                        data-bs-toggle="modal" data-bs-target="#kt_modal_add_user">
                                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Plus.svg-->
                                                        <span class="svg-icon svg-icon-2">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <rect fill="#000000" x="4" y="11" width="16" height="2"
                                                                    rx="1" />
                                                                <rect fill="#000000" opacity="0.5"
                                                                    transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000)"
                                                                    x="4" y="11" width="16" height="2" rx="1" />
                                                            </svg>
                                                        </span>
                                                        Tambah
                                                    </button>
                                                </div>
                                                <!--end::Svg Icon-->
                                                <!--Begin::Col-->
                                                <div class="col">
                                                    <!--begin::Body-->
                                                    <div class="card-body card shadow-sm m-2">
                                                        <div class="table-responsive">
                                                            <table
                                                                class="table table-rounded table-striped border gy-7 gs-7">
                                                                <thead class="headercustom">
                                                                    <tr
                                                                        class="fw-bold fs-6 border-bottom border-gray-200 text-white">
                                                                        <th>No</th>
                                                                        <th>Judul</th>
                                                                        <th>Tipe Knowledge</th>
                                                                        <th>Jenis</th>
                                                                        <th>Status</th>
                                                                        <th>Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>Knowledge</td>
                                                                        <td>Success Story</td>
                                                                        <td>Tacit</td>
                                                                        <td>
                                                                            <p class="text-success">Draft</p>
                                                                        </td>
                                                                        <td>
                                                                            <a type="button" href="#"
                                                                                class="text-hover-warning">
                                                                                <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                <span
                                                                                    class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        width="24px" height="24px"
                                                                                        viewBox="0 0 24 24"
                                                                                        version="1.1">
                                                                                        <path
                                                                                            d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z"
                                                                                            fill="#000000"
                                                                                            fill-rule="nonzero"
                                                                                            transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) " />
                                                                                        <rect fill="#000000"
                                                                                            opacity="0.3" x="5" y="20"
                                                                                            width="15" height="2"
                                                                                            rx="1" />
                                                                                    </svg></span>
                                                                                <!--end::Svg Icon-->
                                                                            </a>
                                                                            <a type="button" href="#"
                                                                                class="text-hover-danger">
                                                                                <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                <!--begin::Svg Icon | path: assets/media/icons/duotone/General/Trash.svg-->
                                                                                <span
                                                                                    class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                        width="24px" height="24px"
                                                                                        viewBox="0 0 24 24"
                                                                                        version="1.1">
                                                                                        <g stroke="none"
                                                                                            stroke-width="1" fill="none"
                                                                                            fill-rule="evenodd">
                                                                                            <rect x="0" y="0" width="24"
                                                                                                height="24" />
                                                                                            <path
                                                                                                d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                                                fill="#000000"
                                                                                                fill-rule="nonzero" />
                                                                                            <path
                                                                                                d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                                                fill="#000000"
                                                                                                opacity="0.3" />
                                                                                        </g>
                                                                                    </svg></span>
                                                                                <!--end::Svg Icon-->
                                                                                <!--end::Svg Icon-->
                                                                            </a>
                                                                            <a type="button" href="#"
                                                                                class="text-hover-success">
                                                                                <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                <span
                                                                                    class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                        width="24px" height="24px"
                                                                                        viewBox="0 0 24 24"
                                                                                        version="1.1">
                                                                                        <g stroke="none"
                                                                                            stroke-width="1" fill="none"
                                                                                            fill-rule="evenodd">
                                                                                            <polygon
                                                                                                points="0 0 24 0 24 24 0 24" />
                                                                                            <path
                                                                                                d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z"
                                                                                                fill="#000000"
                                                                                                fill-rule="nonzero"
                                                                                                transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002) " />
                                                                                        </g>
                                                                                    </svg></span>
                                                                                <!--end::Svg Icon-->
                                                                                <!--end::Svg Icon-->
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>Knowledge</td>
                                                                        <td>Success Story</td>
                                                                        <td>Tacit</td>
                                                                        <td>
                                                                            <p class="text-primary">Review PKP</p>
                                                                        </td>
                                                                        <td>
                                                                            <a type="button" href="#"
                                                                                class="text-hover-primary">
                                                                                <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                <span
                                                                                    class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                        width="24px" height="24px"
                                                                                        viewBox="0 0 24 24"
                                                                                        version="1.1">
                                                                                        <g stroke="none"
                                                                                            stroke-width="1" fill="none"
                                                                                            fill-rule="evenodd">
                                                                                            <rect x="0" y="0" width="24"
                                                                                                height="24" />
                                                                                            <path
                                                                                                d="M13.6855025,18.7082217 C15.9113859,17.8189707 18.682885,17.2495635 22,17 C22,16.9325178 22,13.1012863 22,5.50630526 L21.9999762,5.50630526 C21.9999762,5.23017604 21.7761292,5.00632908 21.5,5.00632908 C21.4957817,5.00632908 21.4915635,5.00638247 21.4873465,5.00648922 C18.658231,5.07811173 15.8291155,5.74261533 13,7 C13,7.04449645 13,10.79246 13,18.2438906 L12.9999854,18.2438906 C12.9999854,18.520041 13.2238496,18.7439052 13.5,18.7439052 C13.5635398,18.7439052 13.6264972,18.7317946 13.6855025,18.7082217 Z"
                                                                                                fill="#000000" />
                                                                                            <path
                                                                                                d="M10.3144829,18.7082217 C8.08859955,17.8189707 5.31710038,17.2495635 1.99998542,17 C1.99998542,16.9325178 1.99998542,13.1012863 1.99998542,5.50630526 L2.00000925,5.50630526 C2.00000925,5.23017604 2.22385621,5.00632908 2.49998542,5.00632908 C2.50420375,5.00632908 2.5084219,5.00638247 2.51263888,5.00648922 C5.34175439,5.07811173 8.17086991,5.74261533 10.9999854,7 C10.9999854,7.04449645 10.9999854,10.79246 10.9999854,18.2438906 L11,18.2438906 C11,18.520041 10.7761358,18.7439052 10.4999854,18.7439052 C10.4364457,18.7439052 10.3734882,18.7317946 10.3144829,18.7082217 Z"
                                                                                                fill="#000000"
                                                                                                opacity="0.3" />
                                                                                        </g>
                                                                                    </svg></span>
                                                                                <!--end::Svg Icon-->
                                                                                <!--end::Svg Icon-->
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="d-flex flex-stack flex-wrap p-4">
                                                            <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of
                                                                50 entries</div>
                                                            <!--begin::Pages-->
                                                            <ul class="pagination">
                                                                <li class="page-item previous">
                                                                    <a href="#" class="page-link">
                                                                        <i class="previous"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="page-item active">
                                                                    <a href="#" class="page-link">1</a>
                                                                </li>
                                                                <li class="page-item">
                                                                    <a href="#" class="page-link">2</a>
                                                                </li>
                                                                <li class="page-item">
                                                                    <a href="#" class="page-link">3</a>
                                                                </li>
                                                                <li class="page-item">
                                                                    <a href="#" class="page-link">4</a>
                                                                </li>
                                                                <li class="page-item">
                                                                    <a href="#" class="page-link">5</a>
                                                                </li>
                                                                <li class="page-item">
                                                                    <a href="#" class="page-link">6</a>
                                                                </li>
                                                                <li class="page-item next">
                                                                    <a href="#" class="page-link">
                                                                        <i class="next"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <!--end::Pages-->
                                                        </div>
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--end::Tables Widget 9-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Post-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Root-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <!--begin::Svg Icon | path: icons/duotone/Navigation/Up-2.svg-->
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.5" x="11" y="10" width="2" height="10" rx="1" />
                    <path
                        d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                        fill="#000000" fill-rule="nonzero" />
                </g>
            </svg>
        </span>
        <!--end::Svg Icon-->
    </div>
    <!--end::Scrolltop-->
    <!--end::Main-->
    @include('layout.footer')
</body>
<!--end::Body-->

</html>
