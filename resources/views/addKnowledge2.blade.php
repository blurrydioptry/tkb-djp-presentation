<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <title>Dev |TKB DJP</title>

    @include('layout.header')

</head>


<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    style="background-image: url(assets/media/patterns/gedung_djp.jpg); background-repeat: no-repeat; background-size: cover; background-position: center; height: 100%; background-attachment:fixed;"
    class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-2">
                    <!--begin::Post-->
                    <div class="content flex-row-fluid" id="kt_content">
                        <!--begin::Row-->
                        <div class="row gy-5 g-xl-4">
                            <!--begin::Col-->
                            <div class="col-xl">
                                <!--begin::Tables Widget 9-->
                                <div class="card card-xxl-stretch mb-xl-8">
                                    <div class="col-md-12">
                                        <!--begin::Body-->
                                        <div class="card shadow-sm m-2">
                                            <div class="card-header headercustom">
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bolder fs-3 text-white">Add
                                                        Knowledge</span>
                                                </h3>
                                            </div>
                                            <div class="row">
                                                <div class="col-md">
                                                    <!--begin::Body-->
                                                    <div class="card-body card shadow-sm m-2">
                                                        <!--begin::Col-->
                                                        <div class="row">
                                                            <!--end::Col-->
                                                            <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Tipe
                                                                Knowledge</label>
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <select class="form-control form-control-solid"
                                                                    name='casename' />
                                                                <option value="">- Pilih Tipe -</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                </select>
                                                            </div>
                                                            <!--end::Input group-->
                                                            <!--begin::Input group-->
                                                            <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Upload Media</label>
                                                            <div class="mb-5">
                                                                <input type="file" class="form-control form-control-solid" name="file">
                                                            </div>
                                                            <!--end::Input group-->
                                                            <div class="col-sm-6 p-2">
                                                                <!--end::Col-->
                                                                <label
                                                                    class="form-label fs-6 fw-bolder text-gray-700 mb-3">Proses
                                                                    Bisnis</label>
                                                                <!--begin::Input group-->
                                                                <div class="mb-5">
                                                                    <select class="form-control form-control-solid"
                                                                        name='casename' />
                                                                    <option value="">- Pilih Probis -</option>
                                                                    <option value="">1</option>
                                                                    <option value="">2</option>
                                                                    <option value="">3</option>
                                                                    </select>
                                                                </div>
                                                                <!--end::Input group-->
                                                                <!--begin::Input group-->
                                                                <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Bisnis Sektor</label>
                                                                <!--begin::Input group-->
                                                                <div class="mb-5">
                                                                    <select class="form-control form-control-solid"
                                                                        name='casename' />
                                                                    <option value="">- Pilih Bisnis Sektor -</option>
                                                                    <option value="">1</option>
                                                                    <option value="">2</option>
                                                                    <option value="">3</option>
                                                                    </select>
                                                                </div>
                                                                <!--end::Input group-->
                                                            </div>
                                                            <!--begin::Col-->
                                                            <div class="col-sm-6 p-2">
                                                                <!--begin::Input group-->
                                                                <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Case Name</label>
                                                                <!--begin::Input group-->
                                                                <div class="mb-5">
                                                                    <select class="form-control form-control-solid"
                                                                        name='subcase' />
                                                                    <option value="">- Pilih Case -</option>
                                                                    <option value="">1</option>
                                                                    <option value="">2</option>
                                                                    <option value="">3</option>
                                                                    </select>
                                                                </div>
                                                                <!--end::Input group-->
                                                                <!--begin::Input group-->
                                                                <label
                                                                    class="form-label fs-6 fw-bolder text-gray-700 mb-3">Sub Case</label>
                                                                <!--begin::Input group-->
                                                                <div class="mb-5">
                                                                    <select class="form-control form-control-solid"
                                                                        name='bisnissektor' />
                                                                    <option value="">- Pilih Sub Case -</option>
                                                                    <option value="">1</option>
                                                                    <option value="">2</option>
                                                                    <option value="">3</option>
                                                                    </select>
                                                                </div>
                                                                <!--end::Input group-->
                                                            </div>
                                                            <!--end::Col-->
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <textarea class="form-control form-control-solid" name="" id="" placeholder="Judul"></textarea>
                                                            </div>
                                                            <!--end::Col-->
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <textarea class="form-control form-control-solid" name="" id="" placeholder="Pendahuluan"></textarea>
                                                            </div>
                                                            <!--end::Col-->
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <textarea class="form-control form-control-solid" name="" id="" placeholder="Daftar Isi"></textarea>
                                                            </div>
                                                            <!--end::Col-->
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <textarea class="form-control form-control-solid" name="" id="" placeholder="Definisi"></textarea>
                                                            </div>
                                                            <!--end::Col-->
                                                            <!--begin::Input group-->
                                                            <div class="mb-5">
                                                                <textarea class="form-control form-control-solid" name="" id="" placeholder="Tagging"></textarea>
                                                            </div>
                                                            <!--end::Col-->
                                                            <div class="separator my-2"></div>
                                                            <div class="text-center px-5">
                                                                <a href="#" class="btn btn-primary px-5">Kembali</a>
                                                                <a href="#" class="btn btn-warning px-5">Simpan dan Lanjutkan</a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                                <!--end::Col-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Body-->
                                </div>
                                <!--end::Tables Widget 9-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Post-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Root-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <!--begin::Svg Icon | path: icons/duotone/Navigation/Up-2.svg-->
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.5" x="11" y="10" width="2" height="10" rx="1" />
                    <path
                        d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                        fill="#000000" fill-rule="nonzero" />
                </g>
            </svg>
        </span>
        <!--end::Svg Icon-->
    </div>
    <!--end::Scrolltop-->
    <!--end::Main-->
    @include('layout.footer')
</body>
<!--end::Body-->

</html>
