        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src="{{url('assets')}}/plugins/global/plugins.bundle.js"></script>
        <script src="{{url('assets')}}/js/scripts.bundle.js"></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Vendors Javascript(used by this page)-->
        <script src="{{url('assets')}}/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
        <!--end::Page Vendors Javascript-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src="{{url('assets')}}/js/custom/widgets.js"></script>
		<script src="{{url('assets')}}/js/custom/apps/chat/chat.js"></script>
		<script src="{{url('assets')}}/js/custom/modals/create-app.js"></script>
		<script src="{{url('assets')}}/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
        <script src="{{ url('vendor') }}/jquery-validation/dist/jquery.validate.js"></script>
        <script src="{{ url('vendor') }}/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{ url('assets/') }}/js/global.js"></script>
        <!--begin::Page Vendors Javascript(used by this page)-->
		<script src="{{ url('assets/') }}/plugins/custom/prismjs/prismjs.bundle.js"></script>
		<script src="{{ url('assets/') }}/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="{{ url('assets/') }}/js/custom/documentation/documentation.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->

<script>
    function swalError(message) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Gagal',
            text: message,
            showConfirmButton: false,
            timer: 3000
        });
    }

    function swalSuccess(message) {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Berhasil',
            text: message,
            showConfirmButton: false,
            timer: 2000
        });
    }

</script>

@yield('js')
