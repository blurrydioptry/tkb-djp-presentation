<div id="carouselExampleCaptions" class="container carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
            class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
            aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
            aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{url('assets')}}/media/misc/image1.png" class="d-block w-100" style="border-radius: 0px 0px 8px 8px;"alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="card-body pt-20 pb-20">
                    <!--begin::Title-->
                    <div class="d-flex align-items-center">
                        <h1 class="fw-bold me-3 text-white">Pencarian Knowledge</h1>
                        <span class="fw-bold text-white opacity-50">Knowledge Management</span>
                    </div>
                    <!--end::Title-->
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column">
                        <!--begin::Block-->
                        <div class="d-lg-flex align-lg-items-center">
                            <!--begin::Simple form-->
                            <div class="rounded d-flex flex-column flex-lg-row align-items-lg-center bg-white p-5 w-xxl-600px h-lg-60px me-lg-10 my-5">
                                <!--begin::Row-->
                                <div class="row flex-grow-1 mb-5 mb-lg-0">
                                    <!--begin::Col-->
                                    <div class="col-lg-7 d-flex align-items-center mb-3 mb-lg-0">
                                        <!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
                                        <span class="svg-icon svg-icon-1 svg-icon-green-400 me-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-flush flex-grow-1" name="search" value="" placeholder="Search the Knowledge Base">
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-lg-4 d-flex align-items-center mb-5 mb-lg-0">
                                        <!--begin::Desktop separartor-->
                                        <div class="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
                                        <!--end::Desktop separartor-->
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-1 svg-icon-gray-400 me-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <!--begin::Select-->
                                        <select class="form-select border-0 flex-grow-1 select2-hidden-accessible" data-control="select2" data-placeholder="Category" data-hide-search="true" data-select2-id="select2-data-7-epk9" tabindex="-1" aria-hidden="true">
                                            <option value=""></option>
                                            <option value="1" selected="selected" data-select2-id="select2-data-9-yhxm">Category</option>
                                            <option value="2">In Progress</option>
                                            <option value="3">Done</option>
                                        </select>

                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Row-->
                                <!--begin::Action-->
                                <div class="min-w-150px text-end">
                                    <button type="submit" class="btn btn-success" id="kt_advanced_search_button_1">Search</button>
                                </div>
                                <!--end::Action-->
                            </div>
                            <!--end::Simple form-->
                        </div>
                        <!--end::Block-->
                    </div>
                    <!--end::Wrapper-->
                </div>
            </div>
        </div>
        <div class="carousel-item">
           <img src="{{url('assets')}}/media/misc/image1.png" class="d-block w-100" style="border-radius: 0px 0px 8px 8px;" alt="...">
        </div>
        <div class="carousel-item">
           <img src="{{url('assets')}}/media/misc/image1.png" class="d-block w-100" style="border-radius: 0px 0px 8px 8px;" alt="...">
        </div>
    </div>
</div>
