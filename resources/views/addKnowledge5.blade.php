<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <title>Dev |TKB DJP</title>

    @include('layout.header')

</head>


<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    style="background-image: url(assets/media/patterns/gedung_djp.jpg); background-repeat: no-repeat; background-size: cover; background-position: center; height: 100%; background-attachment:fixed;"
    class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-2">
                    <!--begin::Post-->
                    <div class="content flex-row-fluid" id="kt_content">
                        <!--begin::Row-->
                        <div class="row gy-5 g-xl-4">
                            <!--begin::Col-->
                            <div class="col-xl">
                                <!--begin::Tables Widget 9-->
                                <div class="card card-xxl-stretch mb-xl-8">
                                    <!--begin::Header-->
                                    <div class="col-md-12">
                                        <!--begin::Body-->
                                        <div class="card shadow-sm m-2">
                                            <div class="card-header headercustom">
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bolder fs-3 text-white">Add
                                                        Knowledge</span>
                                                </h3>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card shadow-sm m-2">
                                                    <div class="card-header headercustom">
                                                        <h3 class="card-title align-items-start flex-column">
                                                            <span class="card-label fw-bolder fs-4 text-white">Peraturan Terkait</span>
                                                        </h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="text-end p-4">
                                                            <button type="button" class="btn btn-sm btn-primary"
                                                                data-bs-toggle="modal" data-bs-target="#kt_modal_add_user">
                                                                <!--begin::Svg Icon | path: icons/duotone/Navigation/Plus.svg-->
                                                                <span class="svg-icon svg-icon-2">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                        xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                        height="24px" viewBox="0 0 24 24" version="1.1">
                                                                        <rect fill="#000000" x="4" y="11" width="16" height="2"
                                                                            rx="1" />
                                                                        <rect fill="#000000" opacity="0.5"
                                                                            transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000)"
                                                                            x="4" y="11" width="16" height="2" rx="1" />
                                                                    </svg>
                                                                </span>
                                                                Tambah
                                                            </button>
                                                        </div>
                                                        <!--end::Svg Icon-->
                                                        <!--Begin::Col-->
                                                        <div class="col">
                                                            <!--begin::Body-->
                                                            <div class="table-responsive p-4">
                                                                <table
                                                                    class="table table-rounded table-striped border gy-7 gs-7">
                                                                    <thead class="headercustom text-center">
                                                                        <tr
                                                                            class="fw-bold fs-6 text-white border-bottom border-gray-200">
                                                                            <th>No</th>
                                                                            <th>Judul Peraturan</th>
                                                                            <th>Aksi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody class="text-center">
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>Knowledge</td>
                                                                            <td>
                                                                                <a type="button" href="#"
                                                                                    class="text-hover-danger">
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/General/Trash.svg-->
                                                                                    <span
                                                                                        class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                            width="24px" height="24px"
                                                                                            viewBox="0 0 24 24"
                                                                                            version="1.1">
                                                                                            <g stroke="none"
                                                                                                stroke-width="1" fill="none"
                                                                                                fill-rule="evenodd">
                                                                                                <rect x="0" y="0" width="24"
                                                                                                    height="24" />
                                                                                                <path
                                                                                                    d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                                                    fill="#000000"
                                                                                                    fill-rule="nonzero" />
                                                                                                <path
                                                                                                    d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                                                    fill="#000000"
                                                                                                    opacity="0.3" />
                                                                                            </g>
                                                                                        </svg></span>
                                                                                    <!--end::Svg Icon-->
                                                                                    <!--end::Svg Icon-->
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2</td>
                                                                            <td>Knowledge</td>
                                                                            <td>
                                                                                <a type="button" href="#"
                                                                                    class="text-hover-danger">
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/General/Trash.svg-->
                                                                                    <span
                                                                                        class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                            width="24px" height="24px"
                                                                                            viewBox="0 0 24 24"
                                                                                            version="1.1">
                                                                                            <g stroke="none"
                                                                                                stroke-width="1" fill="none"
                                                                                                fill-rule="evenodd">
                                                                                                <rect x="0" y="0" width="24"
                                                                                                    height="24" />
                                                                                                <path
                                                                                                    d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                                                    fill="#000000"
                                                                                                    fill-rule="nonzero" />
                                                                                                <path
                                                                                                    d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                                                    fill="#000000"
                                                                                                    opacity="0.3" />
                                                                                            </g>
                                                                                        </svg></span>
                                                                                    <!--end::Svg Icon-->
                                                                                    <!--end::Svg Icon-->
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="d-flex flex-stack flex-wrap p-6">
                                                                <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of
                                                                    50 entries</div>
                                                                <!--begin::Pages-->
                                                                <ul class="pagination">
                                                                    <li class="page-item previous">
                                                                        <a href="#" class="page-link">
                                                                            <i class="previous"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="page-item active">
                                                                        <a href="#" class="page-link">1</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">2</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">3</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">4</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">5</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">6</a>
                                                                    </li>
                                                                    <li class="page-item next">
                                                                        <a href="#" class="page-link">
                                                                            <i class="next"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <!--end::Pages-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card shadow-sm m-2">
                                                    <div class="card-header headercustom">
                                                        <h3 class="card-title align-items-start flex-column">
                                                            <span class="card-label fw-bolder fs-4 text-white">Upload File</span>
                                                        </h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="text-end p-4">
                                                            <button type="button" class="btn btn-sm btn-primary"
                                                                data-bs-toggle="modal" data-bs-target="#kt_modal_add_user">
                                                                <!--begin::Svg Icon | path: icons/duotone/Navigation/Plus.svg-->
                                                                <span class="svg-icon svg-icon-2">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                        xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                        height="24px" viewBox="0 0 24 24" version="1.1">
                                                                        <rect fill="#000000" x="4" y="11" width="16" height="2"
                                                                            rx="1" />
                                                                        <rect fill="#000000" opacity="0.5"
                                                                            transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000)"
                                                                            x="4" y="11" width="16" height="2" rx="1" />
                                                                    </svg>
                                                                </span>
                                                                Tambah
                                                            </button>
                                                        </div>
                                                        <!--end::Svg Icon-->
                                                        <!--Begin::Col-->
                                                        <div class="col">
                                                            <!--begin::Body-->
                                                            <div class="table-responsive p-4">
                                                                <table
                                                                    class="table table-rounded table-striped border gy-7 gs-7">
                                                                    <thead class="headercustom text-center">
                                                                        <tr
                                                                            class="fw-bold fs-6 text-white border-bottom border-gray-200">
                                                                            <th>No</th>
                                                                            <th>Nama File</th>
                                                                            <th>Aksi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody class="text-center">
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>Knowledge</td>
                                                                            <td>
                                                                                <a type="button" href="#"
                                                                                    class="text-hover-danger">
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/General/Trash.svg-->
                                                                                    <span
                                                                                        class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                            width="24px" height="24px"
                                                                                            viewBox="0 0 24 24"
                                                                                            version="1.1">
                                                                                            <g stroke="none"
                                                                                                stroke-width="1" fill="none"
                                                                                                fill-rule="evenodd">
                                                                                                <rect x="0" y="0" width="24"
                                                                                                    height="24" />
                                                                                                <path
                                                                                                    d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                                                    fill="#000000"
                                                                                                    fill-rule="nonzero" />
                                                                                                <path
                                                                                                    d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                                                    fill="#000000"
                                                                                                    opacity="0.3" />
                                                                                            </g>
                                                                                        </svg></span>
                                                                                    <!--end::Svg Icon-->
                                                                                    <!--end::Svg Icon-->
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2</td>
                                                                            <td>Knowledge</td>
                                                                            <td>
                                                                                <a type="button" href="#"
                                                                                    class="text-hover-danger">
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/Design/Edit.svg-->
                                                                                    <!--begin::Svg Icon | path: assets/media/icons/duotone/General/Trash.svg-->
                                                                                    <span
                                                                                        class="svg-icon svg-icon-muted svg-icon-2hx"><svg
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                            width="24px" height="24px"
                                                                                            viewBox="0 0 24 24"
                                                                                            version="1.1">
                                                                                            <g stroke="none"
                                                                                                stroke-width="1" fill="none"
                                                                                                fill-rule="evenodd">
                                                                                                <rect x="0" y="0" width="24"
                                                                                                    height="24" />
                                                                                                <path
                                                                                                    d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                                                    fill="#000000"
                                                                                                    fill-rule="nonzero" />
                                                                                                <path
                                                                                                    d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                                                    fill="#000000"
                                                                                                    opacity="0.3" />
                                                                                            </g>
                                                                                        </svg></span>
                                                                                    <!--end::Svg Icon-->
                                                                                    <!--end::Svg Icon-->
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="d-flex flex-stack flex-wrap p-6">
                                                                <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of
                                                                    50 entries</div>
                                                                <!--begin::Pages-->
                                                                <ul class="pagination">
                                                                    <li class="page-item previous">
                                                                        <a href="#" class="page-link">
                                                                            <i class="previous"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="page-item active">
                                                                        <a href="#" class="page-link">1</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">2</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">3</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">4</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">5</a>
                                                                    </li>
                                                                    <li class="page-item">
                                                                        <a href="#" class="page-link">6</a>
                                                                    </li>
                                                                    <li class="page-item next">
                                                                        <a href="#" class="page-link">
                                                                            <i class="next"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <!--end::Pages-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sparator me-4 mb-4"></div>
                                            <div class="text-end me-4 mb-4">
                                                <a href="#" class="btn btn-primary px-5">Kembali</a>
                                                <a href="#" class="btn btn-warning px-5">Simpan</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--end::Tables Widget 9-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Post-->
                </div>
                <!--end::Container-->
                <!--begin::Footer-->
                <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
                    <!--begin::Container-->
                    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-bold me-1">{{ date('Y') }}&nbsp;©&nbsp;Direktorat Jenderal
                                Pajak</span>
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Menu-->
                        <ul class="menu menu-gray-600 fw-bold order-1">
                            <li class="menu-item">
                                <a href="https://keenthemes.com" target="_blank" class="menu-link px-2">Ketentuan</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://keenthemes.com" target="_blank" class="menu-link px-2">FAQ Aplikasi</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://keenthemes.com/support" target="_blank"
                                    class="menu-link px-2">Panduan</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Hubungi
                                    Kami</a>
                            </li>
                        </ul>
                        <!--end::Menu-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Root-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <!--begin::Svg Icon | path: icons/duotone/Navigation/Up-2.svg-->
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.5" x="11" y="10" width="2" height="10" rx="1" />
                    <path
                        d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                        fill="#000000" fill-rule="nonzero" />
                </g>
            </svg>
        </span>
        <!--end::Svg Icon-->
    </div>
    <!--end::Scrolltop-->
    <!--end::Main-->
    @include('layout.footer')
</body>
<!--end::Body-->

</html>
