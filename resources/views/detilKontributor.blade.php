@extends('layout.master')
@section('content')
<!--begin::Toolbar-->
{{-- @include('layout.carousel') --}}
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-4">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-16">
            <!--begin::Col-->
            <div class="col-xl-12">
                <!--begin::Tables Widget 9-->
                <div class="card card-xxl-stretch mb-5 mb-xl-8">
                    <div class="col-md-12">
                        <!--begin::Body-->
                        <div class="card shadow-sm m-2">
                            <div class="card-header headercustom">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bolder fs-3 text-white">Detil Kontributor</span>
                                </h3>
                            </div>

                            <div class="card-body pt-9 pb-0">
                                <!--begin::Details-->
                                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                                    <!--begin: Pic-->
                                    <div class="me-7 mb-4">
                                        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                                            <img src="assets/media/avatars/150-26.jpg" alt="image">
                                        </div>
                                    </div>
                                    <!--end::Pic-->
                                    <!--begin::Info-->
                                    <div class="flex-grow-1">
                                        <!--begin::Title-->
                                        <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                                            <!--begin::User-->
                                            <div class="d-flex flex-column">
                                                <!--begin::Name-->
                                                <div class="d-flex align-items-center mb-2">
                                                    <a href="#"
                                                        class="text-gray-900 text-hover-primary fs-2 fw-bolder me-1">Patriot
                                                        Roma Angga</a>
                                                    <a href="#">
                                                        <!--begin::Svg Icon | path: icons/duotone/Design/Verified.svg-->
                                                        <span class="svg-icon svg-icon-1 svg-icon-primary">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <path
                                                                    d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                                                    fill="#00A3FF"></path>
                                                                <path class="permanent"
                                                                    d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                                                    fill="white"></path>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </a>
                                                </div>
                                                <!--end::Name-->
                                                <!--begin::Info-->
                                                <div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                                                    <a href="#"
                                                        class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                                        <!--begin::Svg Icon | path: icons/duotone/General/User.svg-->
                                                        <span class="svg-icon svg-icon-4 me-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path
                                                                        d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                                                        fill="#000000" fill-rule="nonzero"
                                                                        opacity="0.3"></path>
                                                                    <path
                                                                        d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                                                        fill="#000000" fill-rule="nonzero"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->1985061502991245123</a>
                                                    <a href="#"
                                                        class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                                        <!--begin::Svg Icon | path: icons/duotone/Map/Marker1.svg-->
                                                        <!--begin::Svg Icon | path: assets/media/icons/duotone/Home/Home.svg-->
                                                        <span class="svg-icon svg-icon-4 me-1"><svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path
                                                                        d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z"
                                                                        fill="#000000" />
                                                                </g>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->KPP Pratama Sumbawa Utara</a>
                                                    <a href="#"
                                                        class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">
                                                        <!--begin::Svg Icon | path: icons/duotone/Communication/Mail-at.svg-->
                                                        <!--begin::Svg Icon | path: assets/media/icons/duotone/Communication/Mail.svg-->
                                                        <span class="svg-icon svg-icon-4 me-1"><svg
                                                                xmlns="http://www.w3.org/2000/svg" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <path
                                                                    d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                                                    fill="#000000" />
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->patriot.angga@pajak.go.id</a>
                                                </div>
                                                <!--end::Info-->
                                            </div>
                                            <!--end::User-->
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Stats-->
                                        <div class="d-flex flex-wrap flex-stack">
                                            <!--begin::Wrapper-->
                                            <div class="d-flex flex-column flex-grow-1 pe-8">
                                                <!--begin::Stats-->
                                                <div class="d-flex flex-wrap">
                                                    <!--begin::Stat-->
                                                    <div
                                                        class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                                        <!--begin::Number-->
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
                                                            <span class="svg-icon svg-icon-3 svg-icon-success me-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                        fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                        <rect fill="#000000" opacity="0.5" x="11" y="5"
                                                                            width="2" height="14" rx="1"></rect>
                                                                        <path
                                                                            d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                                                                            fill="#000000" fill-rule="nonzero"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                            <div class="fs-2 fw-bolder counted" data-kt-countup="true"
                                                                data-kt-countup-value="4500" data-kt-countup-prefix="$">
                                                                100 Konten</div>
                                                        </div>
                                                        <!--end::Number-->
                                                        <!--begin::Label-->
                                                        <div class="fw-bold fs-6 text-gray-400">Total Konten</div>
                                                        <!--end::Label-->
                                                    </div>
                                                    <!--end::Stat-->
                                                </div>
                                                <!--end::Stats-->
                                            </div>
                                            <!--end::Wrapper-->
                                        </div>
                                        <!--end::Stats-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Details-->
                            </div>

                        </div>
                        <!--end::Body-->
                        <div class="col-md-12">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Daftar Pengetahuan yang Telah
                                            Dipublikasikan</span>
                                    </h3>
                                </div>
                                <div class="row">
                                    <!--Begin::Col-->
                                    <div class="col">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="table-responsive">
                                                <table class="table table-rounded table-striped border gy-7 gs-7">
                                                    <thead>
                                                        <tr
                                                            class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                                                            <th>No</th>
                                                            <th>Judul</th>
                                                            <th>Tipe Pengetahuan</th>
                                                            <th>Probis Terkait</th>
                                                            <th>Tanggal Publikasi</th>
                                                            <th>Rating</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>System Architect</td>
                                                            <td>SOP</td>
                                                            <td>Aturan</td>
                                                            <td>2011/04/25</td>
                                                            <td><div class="rating">
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                            </div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Accountant</td>
                                                            <td>Other</td>
                                                            <td>Other</td>
                                                            <td>2011/07/25</td>
                                                            <td><div class="rating">
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2 checked">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                                <div class="rating-label me-2">
                                                                    <i class="bi bi-star fs-2"></i>
                                                                </div>
                                                            </div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="d-flex flex-stack flex-wrap p-4">
                                                <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
                                                <!--begin::Pages-->
                                                <ul class="pagination">
                                                    <li class="page-item previous">
                                                        <a href="#" class="page-link">
                                                            <i class="previous"></i>
                                                        </a>
                                                    </li>
                                                    <li class="page-item active">
                                                        <a href="#" class="page-link">1</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a href="#" class="page-link">2</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a href="#" class="page-link">3</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a href="#" class="page-link">4</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a href="#" class="page-link">5</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a href="#" class="page-link">6</a>
                                                    </li>
                                                    <li class="page-item next">
                                                        <a href="#" class="page-link">
                                                            <i class="next"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!--end::Pages-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Tables Widget 9-->
    </div>
    <!--end::Col-->
</div>
<!--end::Tables Widget 9-->


@endsection
