@extends('layout.master')
@section('content')
<!--begin::Toolbar-->
{{-- @include('layout.carousel') --}}
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-4">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xl-12">
                <!--begin::Tables Widget 9-->
                <div class="card card-xxl-stretch mb-5 mb-xl-8">
                    <div class="row">
                        <div class="col-md-8">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Daftar Peraturan Terbaru</span>
                                    </h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-peraturan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021  Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021  Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021  Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <div class="d-flex flex-stack flex-wrap p-4">
                                    <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
                                    <!--begin::Pages-->
                                    <ul class="pagination">
                                        <li class="page-item previous">
                                            <a href="#" class="page-link">
                                                <i class="previous"></i>
                                            </a>
                                        </li>
                                        <li class="page-item active">
                                            <a href="#" class="page-link">1</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link">2</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link">3</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link">4</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link">5</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link">6</a>
                                        </li>
                                        <li class="page-item next">
                                            <a href="#" class="page-link">
                                                <i class="next"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <!--end::Pages-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-4">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Pencarian Peraturan</span>
                                    </h3>
                                </div>
                                <div class="card-body" style="width: 30rem;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Kata Kunci</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Kata kunci ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Topik Peraturan</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Topik Peraturan ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Jenis Peraturan</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Jenis Peraturan ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Nomor</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Nomor ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Tanggal</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Tanggal ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Tahun</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Tahun ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Bisnis Sektor</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Bisnis Sektor ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Proses Bisnis</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Proses Bisnis ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="form-label fs-6 fw-bolder text-gray-700">Case Name</label>
                                            <div class="mb-5">
                                                <input type="text" class="form-control form-control-solid" placeholder="Case Name ..." />
                                            </div>
                                        </div>
                                        <div class="sparator my-2"></div>
                                        <div class="text-center px-5">
                                            <a href="#" class="btn btn-primary px-5">Reset</a>
                                            <a href="#" class="btn btn-warning px-5">Cari</a>
                                        </div>


                                        <!--end::Col-->
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
                <!--end::Tables Widget 9-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
    <!--end::Post-->
</div>
<!--end::Container-->

    @endsection
