@extends('layout.master')
@section('content')
<!--begin::Toolbar-->
<!--end::Carousel-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-4">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-4">
            <!--begin::Col-->
            <div class="col-xl">
                <!--begin::Tables Widget 9-->
                <div class="card card-xxl-stretch mb-xl-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Search Knowledge</span>
                                    </h3>
                                </div>
                                <!--begin::Body-->
                                <div class="row">
                                    <div class="col">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <!--begin::Col-->
                                            <div class="row">
                                                <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Kata Kunci</label>
                                                <!--begin::Input group-->
                                                <div class="mb-5">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Kata kunci ..." />
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Jenis Pengetahuan</label>
                                                <div class="row">
                                                        <div class="col-sm-2 mb-5">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="radio" name="radio1" value=""/>
                                                                <label class="form-check-label">
                                                                    Success Story
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 mb-5">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="radio" name="radio1" value=""/>
                                                                <label class="form-check-label">
                                                                    Know How To
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 mb-5">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="radio" name="radio1" value=""/>
                                                                <label class="form-check-label">
                                                                    Peraturan
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 mb-5">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="radio" name="radio1" value=""/>
                                                                <label class="form-check-label">
                                                                    SOP
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 mb-5">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="radio" name="radio1" value=""/>
                                                                <label class="form-check-label">
                                                                    Other Knowledge
                                                                </label>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!--end::Input group-->
                                                <div class="col-sm-6 p-2">
                                                    <!--end::Col-->
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Proses Bisnis</label>
                                                    <!--begin::Input group-->
                                                    <div class="mb-5">
                                                        <select class="form-control form-control-solid" name='casename' />
                                                        <option value="">- Pilih Probis -</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                        <option value="">3</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input group-->
                                                    <!--begin::Input group-->
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Bisnis Sektor</label>
                                                    <!--begin::Input group-->
                                                    <div class="mb-5">
                                                        <select class="form-control form-control-solid" name='bisnissektor' />
                                                        <option value="">- Pilih Bisnis Sektor -</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                        <option value="">3</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input group-->
                                                </div>
                                                <!--begin::Col-->
                                                <div class="col-sm-6 p-2">
                                                    <!--begin::Input group-->
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Case Name</label>
                                                    <!--begin::Input group-->
                                                    <div class="mb-5">
                                                        <select class="form-control form-control-solid" name='casename' />
                                                        <option value="">- Pilih Case -</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                        <option value="">3</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input group-->
                                                    <!--begin::Input group-->
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Sub Case</label>
                                                    <!--begin::Input group-->
                                                    <div class="mb-5">
                                                        <select class="form-control form-control-solid" name='subcase' />
                                                        <option value="">- Pilih Sub Case -</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                        <option value="">3</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input group-->

                                                </div>
                                                <!--end::Col-->
                                                <div class="separator my-2"></div>
                                                <div class="text-center px-5">
                                                    <a href="#" class="btn btn-primary px-5">Cari</a>
                                                    <a href="#" class="btn btn-warning px-5">Reset</a>
                                                </div>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--begin::Body-->
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Daftar Pengetahuan Terbaru</span>
                                    </h3>
                                </div>
                                <!--begin::Body-->
                                <div class="row">
                                    <div class="col">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                                <div class="textsplit">
                                                    <div class="textsplit">
                                                        <a class="align-items-start flex-column text-dark">13 September</a>

                                                    </div>
                                                    <a class="align-items-start flex-column text-dark">Video</a>

                                                </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                                <div class="textsplit">
                                                    <div class="textsplit">
                                                        <a class="align-items-start flex-column text-dark">13 September</a>

                                                    </div>
                                                    <a class="align-items-start flex-column text-dark">Video</a>

                                                </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                                <div class="textsplit">
                                                    <div class="textsplit">
                                                        <a class="align-items-start flex-column text-dark">13 September</a>

                                                    </div>
                                                    <a class="align-items-start flex-column text-dark">Video</a>

                                                </div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Pagination-->
                                        <!--begin::Body-->
                                        <div class="d-flex flex-stack flex-wrap p-4">
                                            <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
                                            <!--begin::Pages-->
                                            <ul class="pagination">
                                                <li class="page-item previous">
                                                    <a href="#" class="page-link">
                                                        <i class="previous"></i>
                                                    </a>
                                                </li>
                                                <li class="page-item active">
                                                    <a href="#" class="page-link">1</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">4</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">5</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">6</a>
                                                </li>
                                                <li class="page-item next">
                                                    <a href="#" class="page-link">
                                                        <i class="next"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Pages-->
                                        </div>
                                        <!--end::Pagination-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--begin::Body-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Tables Widget 9-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
    <!--end::Post-->
</div>
<!--end::Container-->

@endsection
