@extends('layout.master')
@section('content')
<!--begin::Toolbar-->
@include('layout.carousel')
<!--end::Carousel-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-4">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xl-12">
                <!--begin::Tables Widget 9-->
                <div class="card card-xxl-stretch mb-5 mb-xl-8">
                    <div class="row">
                        <div class="col-md-8">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Pengetahuan Terbaru</span>
                                    </h3>
                                    <a type="button"
                                        class="btn btn-sm btn-icon btn-color-secondary btn-active-light-primary mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lihat Selengkapnya">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-md-6">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <div class="col-md-6">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-md-6">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-pengetahuan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</p>
                                            <div class="separator my-2"></div>
                                            <div class="textsplit">
                                                <a class="align-items-start flex-column text-dark">13 September 2022</a>

                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-4">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Pengetahuan Populer</span>
                                    </h3>
                                    <a type="button"
                                        class="btn btn-sm btn-icon btn-color-secondary btn-active-light-primary mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lihat Selengkapnya">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>
                                </div>
                                <div class="card-body" style="width: 30rem;">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <a href="/detil-pengetahuan" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</a>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-8">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Peraturan Terbaru</span>
                                    </h3>
                                    <a type="button"
                                        class="btn btn-sm btn-icon btn-color-secondary btn-active-light-primary mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lihat Selengkapnya">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="/detil-peraturan"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                        </div>
                                        <!--end::Body-->
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <a href="#"
                                                class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                            <p class="text-dark-75 fw-bold fs-5 m-0">Perubahan Kedua Atas Peraturan Menteri Keuangan
                                                Nomor 31/PMK.010/2021 Tentang Pajak Penjualan Atas Barang Mewah Atas Penyerahan
                                                Barang Kena Pajak Yang Tergolong Mewah Berupa Kendaraan Bermotor Tertentu Yang
                                                Ditanggung Pemerintah Tahun Anggaran 2021</p>
                                            <div class="separator my-2"></div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-4">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Peraturan Populer</span>
                                    </h3>
                                    <a type="button"
                                        class="btn btn-sm btn-icon btn-color-secondary btn-active-light-primary mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lihat Selengkapnya">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>
                                </div>
                                <div class="card-body" style="width: 30rem;">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <a href="/detil-peraturan" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <a href="#" class="card-title fw-bolder text-muted text-hover-primary fs-4">PMK-120/PMK.010/2021</a>
                                        </div>

                                        <!--end::Col-->
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--begin::col-->
                        <div class="col-md-12">
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 text-white">Top Kontributor</span>
                                    </h3>
                                    <a type="button"
                                        class="btn btn-sm btn-icon btn-color-secondary btn-active-light-primary mt-2"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lihat Selengkapnya">
                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>
                                </div>
                                <div class="row">
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                class="card-title fw-bolder text-hover-black fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                class="card-title fw-bolder text-hover-primary fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                    class="card-title fw-bolder text-hover-primary fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                class="card-title fw-bolder text-hover-primary fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                    class="card-title fw-bolder text-hover-primary fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                    <!--Begin::Col-->
                                    <div class="col-md-4">
                                        <!--begin::Body-->
                                        <div class="card-body card shadow-sm m-2">
                                            <div class="d-flex  align-items-center px-3 ">
                                                <!--begin::Avatar-->
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="assets/media/avatars/150-25.jpg" />
                                                </div>
                                                <!--end::Avatar-->
                                                <!--begin::Username-->
                                                <div class="d-flex flex-column">
                                                    <a href="/detil-kontributor"
                                                    class="card-title fw-bolder text-hover-primary fs-5">Patriot Angga</a>
                                                    <div class="d-flex align-items-center text-muted fs-7">patriot.angga@pajak.go.id
                                                    </div>
                                                    <a href="#" class="fw-bold text-muted fs-7">KPP Pratama Bandung</a>
                                                </div>
                                                <!--end::Username-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Col-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::col-->
                    </div>
                </div>
                <!--end::Tables Widget 9-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
    <!--end::Post-->
</div>
<!--end::Container-->

@endsection
