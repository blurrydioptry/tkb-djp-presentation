@extends('layout.master')
@section('content')

<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container mt-4">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xl-12">
                <!--begin::Tables Widget 9-->
                <div class="card card-xxl-stretch mb-5 mb-xl-8">
                    <!--begin::Body-->
                    <div class="row">

                        <div class="col-md-12">
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Detil Pengetahuan</span>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <iframe allowfullscreen="allowfullscreen"
                                        src="https://www.youtube.com/embed/UyDyAE8ji6o?autoplay=0&amp;start=0" width="1200"
                                        height="700" frameborder="0"></iframe>
                                    <div class="separator my-2"></div>
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-1 mb-1">Tutorial Mendapatkan EFIN Melalui Website DJP dan Email</span>
                                    </h3>
                                    <div class="textsplit">

                                        <a class="align-items-start flex-column text-dark fs-5">102x dilihat</a>
                                        <a class="align-items-start flex-column text-dark fs-5">Aktif Knowledge</a>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star fs-2"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star fs-2"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star fs-2"></i>
                                            </div>
                                            <div class="rating-label me-2">
                                                <i class="bi bi-star fs-2"></i>
                                            </div>
                                            <div class="rating-label me-2">
                                                <i class="bi bi-star fs-2"></i>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="separator my-2"></div>
                                    <div class="textsplit">
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">Proses Bisnis</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">Bisnis Sektor</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">Case Name</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">Sub Case</a>
                                    </div>

                                    <div class="separator my-2"></div>
                                    <p class="text-dark-75 fw-bold fs-5 m-0">Pendahuluan Pendahuluan Pendahuluan Pendahuluan
                                        Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan
                                        Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan
                                        Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan
                                        Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan Pendahuluan
                                    </p>
                                    <div class="separator my-2"></div>


                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 mb-1">Daftar Isi</span>
                                    </h3>
                                    <p class="text-dark-75 fw-bold fs-5 m-0">Daftar Isi Daftar Isi Daftar Isi Daftar Isi
                                        Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar
                                        Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi
                                        Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar
                                        Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi
                                        Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar
                                        Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi Daftar Isi </p>
                                    <div class="separator my-2"></div>

                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 mb-1">Definisi</span>
                                    </h3>
                                    <p class="text-dark-75 fw-bold fs-5 m-0">
                                        DefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisiDefinisi
                                    </p>
                                    <div class="separator my-2"></div>

                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 mb-1">Isi Materi</span>
                                    </h3>
                                    <p class="text-dark-75 fw-bold fs-5 m-0">Isi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi
                                        Isi Materi Isi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi
                                        Materi Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi
                                        Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi
                                        Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi Materi Isi
                                        Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi
                                        MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi MateriIsi
                                        MateriIsi MateriIsi Materi Isi Materi Isi Materi Isi Materi Isi Materi Isi Materi
                                        Isi Materi Isi Materi </p>
                                    <div class="separator my-2"></div>


                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder fs-3 mb-1">Tag</span>
                                    </h3>
                                    <div class="textsplit">

                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>
                                        <a href="#" class="align-items-start flex-column text-dark fs-5">#hastagh</a>


                                    </div>

                                </div>
                            </div>


                            <!--end::Body-->
                        </div>

                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Pranala Luar</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Referensi</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Bacaan Lanjutan</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Lihat Pula</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Peraturan Terkait</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>

                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Upload File</span>
                                    </h3>
                                </div>
                                <div class="card-body card shadow-sm m-2">
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 8 TAHUN 1983 TENTANG PAJAK PERTAMBAHAN NILAI BARANG DAN JASA DAN PAJAK
                                        PENJUALAN</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                    <a href="#" class="card-title fw-bolder  text-hover-primary fs-4">UNDANG-UNDANG
                                        NOMOR 17 TAHUN 2003 TENTANG KEUANGAN NEGARA</a>
                                </div>

                            </div>

                            <!--end::Body-->
                        </div>
                        <div class="col-md-12">

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card shadow-sm m-2">
                                <div class="card-header headercustom">
                                    <h3 class="card-title">
                                        <span class="card-label fw-bolder fs-3 text-white">Feedback</span>
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-body card shadow-sm m-2">
                                                <form class="position-relative mb-6">
                                                    <textarea
                                                        class="form-control border-0 p-0 pe-10 resize-none min-h-25px"
                                                        data-kt-autosize="true" rows="1" placeholder="Reply.."
                                                        style="overflow: hidden; overflow-wrap: break-word; height: 25px;"></textarea>
                                                </form>
                                                <!--begin::Separator-->
                                                <div class="separator mb-4"></div>
                                                <div class="text-end">
                                                    <a href="#" class="btn btn-warning px-5">Kirim</a>
                                                </div>
                                                <!--end::Separator-->
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card-body card shadow-sm m-2">
                                                <!--begin::Header-->
                                                <div class="d-flex align-items-center mb-3">
                                                    <!--begin::User-->
                                                    <div class="d-flex align-items-center flex-grow-1">
                                                        <!--begin::Avatar-->
                                                        <div class="symbol symbol-45px me-5">
                                                            <img src="assets/media/avatars/150-10.jpg" alt="">
                                                        </div>
                                                        <!--end::Avatar-->
                                                        <!--begin::Info-->
                                                        <div class="d-flex flex-column">
                                                            <a href="#"
                                                                class="text-gray-900 text-hover-primary fs-6 fw-bolder">Carles
                                                                Nilson</a>
                                                            <span class="text-gray-400 fw-bold">Yestarday at 5:06
                                                                PM</span>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <!--end::User-->
                                                </div>
                                                <!--end::Header-->
                                                <!--begin::Post-->
                                                <div class="mb-7">
                                                    <!--begin::Text-->
                                                    <div class="text-gray-800 mb-5">Outlines keep you honest. They stop
                                                        you from
                                                        indulging in poorly thought-out metaphors about driving and keep
                                                        you focused
                                                        on the overall structure of your post</div>
                                                    <!--end::Text-->
                                                    <!--begin::Toolbar-->
                                                    <div class="d-flex align-items-center mb-5">
                                                        <a href="#"
                                                            class="btn btn-sm btn-light btn-color-muted btn-active-light-success px-4 py-2 me-4">
                                                            <!--begin::Svg Icon | path: icons/duotone/Communication/Group-chat.svg-->
                                                            <span class="svg-icon svg-icon-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px"
                                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <path
                                                                        d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z"
                                                                        fill="#000000"></path>
                                                                    <path
                                                                        d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z"
                                                                        fill="#000000" opacity="0.3"></path>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->12</a>
                                                        <a href="#"
                                                            class="btn btn-sm btn-light btn-color-muted btn-active-light-danger px-4 py-2">
                                                            <!--begin::Svg Icon | path: icons/duotone/General/Heart.svg-->
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                        fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                        <path
                                                                            d="M16.5,4.5 C14.8905,4.5 13.00825,6.32463215 12,7.5 C10.99175,6.32463215 9.1095,4.5 7.5,4.5 C4.651,4.5 3,6.72217984 3,9.55040872 C3,12.6834696 6,16 12,19.5 C18,16 21,12.75 21,9.75 C21,6.92177112 19.349,4.5 16.5,4.5 Z"
                                                                            fill="#000000" fill-rule="nonzero"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->150</a>
                                                    </div>
                                                    <!--end::Toolbar-->
                                                </div>
                                                <!--end::Post-->
                                                <!--begin::Replies-->
                                                <div class="mb-7 ps-10">
                                                    <!--begin::Reply-->
                                                    <div class="d-flex mb-5">
                                                        <!--begin::Avatar-->
                                                        <div class="symbol symbol-45px me-5">
                                                            <img src="assets/media/avatars/150-11.jpg" alt="">
                                                        </div>
                                                        <!--end::Avatar-->
                                                        <!--begin::Info-->
                                                        <div class="d-flex flex-column flex-row-fluid">
                                                            <!--begin::Info-->
                                                            <div class="d-flex align-items-center flex-wrap mb-1">
                                                                <a href="#"
                                                                    class="text-gray-800 text-hover-primary fw-bolder me-2">Alice
                                                                    Danchik</a>
                                                                <span class="text-gray-400 fw-bold fs-7">1 day</span>
                                                                <a href="#"
                                                                    class="ms-auto text-gray-400 text-hover-primary fw-bold fs-7">Reply</a>
                                                            </div>
                                                            <!--end::Info-->
                                                            <!--begin::Post-->
                                                            <span class="text-gray-800 fs-7 fw-normal pt-1">Long before
                                                                you sit dow
                                                                to put digital pen to paper you need to make sure you
                                                                have to sit
                                                                down and write.</span>
                                                            <!--end::Post-->
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <!--end::Reply-->
                                                    <!--begin::Reply-->
                                                    <div class="d-flex">
                                                        <!--begin::Avatar-->
                                                        <div class="symbol symbol-45px me-5">
                                                            <img src="assets/media/avatars/150-8.jpg" alt="">
                                                        </div>
                                                        <!--end::Avatar-->
                                                        <!--begin::Info-->
                                                        <div class="d-flex flex-column flex-row-fluid">
                                                            <!--begin::Info-->
                                                            <div class="d-flex align-items-center flex-wrap mb-1">
                                                                <a href="#"
                                                                    class="text-gray-800 text-hover-primary fw-bolder me-2">Harris
                                                                    Bold</a>
                                                                <span class="text-gray-400 fw-bold fs-7">2 days</span>
                                                                <a href="#"
                                                                    class="ms-auto text-gray-400 text-hover-primary fw-bold fs-7">Reply</a>
                                                            </div>
                                                            <!--end::Info-->
                                                            <!--begin::Post-->
                                                            <span class="text-gray-800 fs-7 fw-normal pt-1">Outlines
                                                                keep you
                                                                honest. They stop you from indulging in poorly</span>
                                                            <!--end::Post-->
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <!--end::Reply-->
                                                </div>
                                                <!--end::Replies-->
                                                <!--begin::Separator-->
                                                <div class="separator mb-4"></div>
                                                <!--end::Separator-->
                                                <!--begin::Reply input-->
                                                <form class="position-relative mb-6">
                                                    <textarea
                                                        class="form-control border-0 p-0 pe-10 resize-none min-h-25px"
                                                        data-kt-autosize="true" rows="1" placeholder="Reply.."
                                                        style="overflow: hidden; overflow-wrap: break-word; height: 25px;"></textarea>
                                                    <div class="position-absolute top-0 end-0 me-n5">
                                                        <span
                                                            class="btn btn-icon btn-sm btn-active-color-primary pe-0 me-2">
                                                            <!--begin::Svg Icon | path: icons/duotone/General/Clip.svg-->
                                                            <span class="svg-icon svg-icon-3 mb-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                        fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                                        <path
                                                                            d="M14,16 L12,16 L12,12.5 C12,11.6715729 11.3284271,11 10.5,11 C9.67157288,11 9,11.6715729 9,12.5 L9,17.5 C9,19.4329966 10.5670034,21 12.5,21 C14.4329966,21 16,19.4329966 16,17.5 L16,7.5 C16,5.56700338 14.4329966,4 12.5,4 L12,4 C10.3431458,4 9,5.34314575 9,7 L7,7 C7,4.23857625 9.23857625,2 12,2 L12.5,2 C15.5375661,2 18,4.46243388 18,7.5 L18,17.5 C18,20.5375661 15.5375661,23 12.5,23 C9.46243388,23 7,20.5375661 7,17.5 L7,12.5 C7,10.5670034 8.56700338,9 10.5,9 C12.4329966,9 14,10.5670034 14,12.5 L14,16 Z"
                                                                            fill="#000000" fill-rule="nonzero"
                                                                            transform="translate(12.500000, 12.500000) rotate(-315.000000) translate(-12.500000, -12.500000)">
                                                                        </path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                        <span class="btn btn-icon btn-sm btn-active-color-primary ps-0">
                                                            <!--begin::Svg Icon | path: icons/duotone/Map/Marker1.svg-->
                                                            <span class="svg-icon svg-icon-2 mb-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                    width="24px" height="24px" viewBox="0 0 24 24"
                                                                    version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none"
                                                                        fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                                        <path
                                                                            d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z"
                                                                            fill="#000000" fill-rule="nonzero"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </div>
                                                </form>
                                                <!--edit::Reply input-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-stack flex-wrap p-4">
                                        <div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
                                        <!--begin::Pages-->
                                        <ul class="pagination">
                                            <li class="page-item previous">
                                                <a href="#" class="page-link">
                                                    <i class="previous"></i>
                                                </a>
                                            </li>
                                            <li class="page-item active">
                                                <a href="#" class="page-link">1</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">2</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">3</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">4</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">5</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="#" class="page-link">6</a>
                                            </li>
                                            <li class="page-item next">
                                                <a href="#" class="page-link">
                                                    <i class="next"></i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!--end::Pages-->
                                    </div>

                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--begin::Body-->
                </div>
                <!--end::Tables Widget 9-->
            </div>
            <!--end::Col-->
        </div>
    </div>
    <!--end::Post-->
</div>
<!--end::Container-->


@endsection('content')
