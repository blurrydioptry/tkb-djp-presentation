<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('beranda');
});

Route::get('/detil-peraturan', function () {
    return view('detilRegulasi');
});

Route::get('/daftar-peraturan', function () {
    return view('daftarRegulasi');
});

Route::get('/detil-kontributor', function () {
    return view('detilKontributor');
});

Route::get('/daftar-pengetahuan', function () {
    return view('daftarPengetahuan');
});

Route::get('/detil-pengetahuan', function () {
    return view('detilPengetahuan');
});


Route::get('/cari-pengetahuan-ctas', function () {
    return view('searchknowledge');
});

Route::get('/cari-pengetahuan/{probis}/{casename}', function () {
    return view('searchknowledge');
});

Route::get('/tambah-pengetahuan', function () {
    return view('addKnowledge');
});

Route::get('/tambah-pengetahuan/detil-success', function () {
    return view('addKnowledge1');
});

Route::get('/tambah-pengetahuan/detil-1', function () {
    return view('addKnowledge2');
});

Route::get('/tambah-pengetahuan/detil-2', function () {
    return view('addKnowledge3');
});

Route::get('/tambah-pengetahuan/detil-3', function () {
    return view('addKnowledge4');
});

Route::get('/tambah-pengetahuan/detil-4', function () {
    return view('addKnowledge5');
});


